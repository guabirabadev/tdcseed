<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPagesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landing_pages', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tempalte_id')->unsigned();
            $table->foreign('tempalte_id')->references('id')->on('templates');
            $table->string('title');
            $table->string('alias');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('languagem');
            $table->enum('published', ['no', 'yes']);
            $table->dateTime('publishAt')->nullable();
            $table->dateTime('unPublishAt')->nullable();
            $table->string('redirectType');
            $table->longText('meta');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landing_pages');
	}

}
