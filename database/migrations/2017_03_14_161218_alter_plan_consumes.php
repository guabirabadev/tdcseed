<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanConsumes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_consumes', function (Blueprint $table) {
            $table->dropForeign('plan_consumes_candidate_id_foreign');
            $table->dropColumn('candidate_id');
            $table->integer('candidate_plan_id')->unsigned();
            $table->foreign('candidate_plan_id')
                ->references('id')
                ->on('candidate_plan')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
