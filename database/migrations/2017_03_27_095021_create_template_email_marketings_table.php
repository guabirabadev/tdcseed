<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateEmailMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_email_marketings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->mediumText('description')->nullable();
            $table->string('content_type');
            $table->longText('content_value');
            $table->timestamps();
            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')
                 ->references('id')
                 ->on('candidates')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('template_email_marketings');
    }
}
