<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->integer('type_events_id')->unsigned()->nullable();
            $table->foreign('type_events_id')->references('id')->on('type_events');
            $table->integer('visibilite_events_id')->unsigned()->nullable();
            $table->foreign('visibilite_events_id')->references('id')->on('visibilite_events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
