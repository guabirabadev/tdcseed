<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->string('file_disk');
            $table->string('file_path');
            $table->string('file_url');
            $table->string('file_ext');
            $table->string('file_size');
            $table->string('file_mime');
            $table->integer('attach_id');
            $table->string('attach_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_files');
    }
}
