<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Consume extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consume', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('description_consume');
            $table->bigInteger('sms_consume');
            $table->bigInteger('email_consume');
            $table->integer('plan_consume_id')->unsigned();
            $table->foreign('plan_consume_id')
                ->references('id')
                ->on('plan_consumes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consume');
    }
}
