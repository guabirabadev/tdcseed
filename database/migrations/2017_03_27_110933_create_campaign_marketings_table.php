<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_marketings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->bigInteger('sender_id')->unsigned();
            $table->integer('template_email_marketing_id')->unsigned();
            $table->bigInteger('list_marketing_id')->unsigned();
            $table->integer('candidate_id')->unsigned();
            $table->dateTime('scheduling')->nullable();
            $table->integer('job_id')->nullable();
            $table->dateTime('last_send')->nullable();
            $table->foreign('sender_id')
                ->references('id')
                ->on('senders');
            $table->foreign('template_email_marketing_id')
                ->references('id')
                ->on('template_email_marketings');
            $table->foreign('list_marketing_id')
                ->references('id')
                ->on('list_marketing');
            $table->foreign('candidate_id')
                ->references('id')
                ->on('candidates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_marketings');
    }
}
