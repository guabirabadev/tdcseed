<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages_tos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('message_id');
            $table->foreign('message_id')->references('id')->on('messages');
            $table->integer('user_to');
            $table->foreign('user_to')->references('id')->on('users');
            $table->integer('readed')->default(0);
            $table->dateTime('readed_at')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages_tos');
	}

}
