<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsMkTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_mks', function(Blueprint $table) {
            $table->increments('id');
            $table->text('profileAvatar')->nullable(); //pode escolher entre gravatar ou upload do arquivo de imagem
            $table->string('title');
            $table->string('firistName');
            $table->string('lastName');
            $table->string('email');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('addressLineOne');
            $table->string('addressLineTwo')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->decimal('attribuition');
            $table->date('attribuitionDate');
            $table->string('phone');
            $table->string('mobile');
            $table->string('fax');
            $table->string('website');
            $table->string('preferedLocale');
            $table->string('timeZone');
            $table->string('stage');
            $table->string('contactOwner');
            $table->text('tags');
            $table->string('twitter');
            $table->string('facebook');
            $table->string('googleplus');
            $table->string('skype');
            $table->string('instagram');
            $table->string('linkedin');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contact_mks');
	}

}
