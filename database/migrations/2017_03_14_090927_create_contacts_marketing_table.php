<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsMarketingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_marketing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email', 255);
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->mediumText('custom_fields')->nullable();
            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')
                ->references('id')
                ->on('candidates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
        Schema::create('contact_marketing_list_marketing', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->bigInteger('list_marketing_id')->unsigned();
            $table->foreign('list_marketing_id')
                ->references('id')
                ->on('list_marketing')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->bigInteger('contact_marketing_id')->unsigned();
            $table->foreign('contact_marketing_id')
                ->references('id')
                ->on('contact_marketing')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_marketing_list_marketing');
        Schema::dropIfExists('contact_marketing');
        Schema::dropIfExists('list_marketing');
    }
}
