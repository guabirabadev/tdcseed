<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        //tabela onde ficam cadastrados os tweets
		Schema::create('tweets', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date_creation');
            $table->integer('id_tweet');
            $table->integer('id_str');
            $table->string('text', 140);
            $table->string('hashtags');
            $table->string('symbols');
            $table->string('user_mentions');
            $table->string('urls');
            $table->string('source');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweets');
	}

}
