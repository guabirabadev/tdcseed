<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('lists', function (Blueprint $table) {
            $table->bigInteger('id')->unique();
            $table->string('name', 255);
            $table->integer('recipient_count');
        });
        Schema::create('list_marketing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('recipient_count')->default(0);
            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')
                ->references('id')
                ->on('candidates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('list_marketing');
    }
}
