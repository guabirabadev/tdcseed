<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('addressLineOne');
            $table->string('addressLineTwo')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('country');
            $table->string('website');
            $table->string('phone');
            $table->string('owner');
            $table->string('annualRevenue')->nullable();
            $table->string('fax')->nullable();
            $table->string('numberOfEmployees')->nullable();
            $table->string('industry')->nullable();
            $table->longText('description')->nullable();
            $table->integer('score')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
