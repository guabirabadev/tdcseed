<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteractionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interactions', function(Blueprint $table) {
            $table->increments('id');
             $table->string('product_name');
            $table->string('product_group');
            $table->string('product_type');
            $table->string('product_unity_price');
            $table->string('product_group_price');
            $table->string('sender_id');
            $table->string('sender_name');
            $table->string('serder_email');
            $table->string('message');
            $table->string('key_foy_api');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interactions');
	}

}
