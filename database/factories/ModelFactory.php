<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Candidate::class, function (Faker\Generator $faker) {

    return [
        'name_political' => $faker->title,
        'membership' => $faker->jobTitle,
        'biograph' => $faker->text(500)
    ];
});

//$factory->define(App\Models\Profile::class, function (Faker\Generator $faker) {
//
//    return [
//        'nickname' => $faker->userName,
//        'genre' => rand(0, 1),
//        'birth' => $faker->date('d/m/Y'),
//        'observations' => $faker->text(500)
//    ];
//});
//
//$factory->define(App\Models\Voter::class, function (Faker\Generator $faker) {
//
//    return [
//        'nickname' => $faker->userName,
//        'genre' => rand(0, 1),
//        'birth' => $faker->date('d/m/Y'),
//        'observations' => $faker->text(500)
//    ];
//});

$factory->define(App\Models\Address::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Models\User::class)->create()->id,
        'street' => $faker->address,
        'number' => $faker->randomDigit,
        'district' => $faker->title,
        'city' => $faker->city,
        'state' => $faker->title,
        'complement' => $faker->title,
        'reference' => $faker->title,
    ];
});

$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {

    return [
        'user_id' => factory(App\Models\User::class)->create()->id,
        'type' => $faker->phoneNumber,
        'contact' => $faker->safeEmail,
        'description' => $faker->text()
    ];
});

$factory->define(App\Models\UserType::class, function (Faker\Generator $faker) {

    return [
        'kind' => $faker->numberBetween(0,15),
        'description' => $faker->text,
        'id_political' => rand(1,5),
    ];
});

$factory->define(App\Plan::class, function (Faker\Generator $faker) {

    return [
        'max_users' => $faker->numberBetween(0,15),
        'max_electors' => $faker->numberBetween(0,15),
        'sms_quantity' => $faker->numberBetween(0,15),
        'emails_quantity' => $faker->numberBetween(1000,50000),
        'price' => $faker->randomFloat(null, 0,200),
        'id_political' => rand(1,5)
    ];
});

$factory->define(App\Messages::class, function (Faker\Generator $faker) {

    return [
        'user_send' => rand(1, 50),
        'title' => $faker->word,
        'body' => $faker->sentence
    ];
});


$factory->define(App\MessagesTo::class, function (Faker\Generator $faker) {

    return [
        'message_id' => rand(1, 50),
        'user_to' => rand(1, 50),
        'readed' => rand(1, 2),
        'readed_at' => $faker->dateTime
    ];
});


$factory->define(App\Attachment::class, function (Faker\Generator $faker) {

    return [
        'message_id' => rand(1, 50),
        'name' => $faker->name,
        'type' => $faker->word,
        'path' => $faker->url,
    ];
});

$factory->define(App\Models\Interactions::class, function (Faker\Generator $faker) {
$palavras = array('Coca-cola', 'delvale', 'fanta', 'coca-zero', 'gatored');
$grupos = array('cervejas', 'sucos', 'refrigerantes', 'energeticos');
$aleatorio = rand(0, 3);
$palavras[$aleatorio];
    return [
        'product_name' => $palavras[$aleatorio],
        'product_group' => $grupos[$aleatorio],
        'product_type' => $faker->word,
        'product_unity_price' => rand(2, 15),
        'product_group_price' => rand(24, 150),
        'sender_id' => rand(123456, 999999),
        'sender_name' => $faker->firstName,
        'serder_email' => $faker->freeEmail,
        'message' => $faker->sentence,
        'key_foy_api' => $palavras[$aleatorio],
    ];
});

