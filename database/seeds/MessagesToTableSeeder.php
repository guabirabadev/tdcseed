<?php

use App\MessagesTo;
use Illuminate\Database\Seeder;

class MessagesToTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MessagesTo::class, 20)->create();
    }
}
