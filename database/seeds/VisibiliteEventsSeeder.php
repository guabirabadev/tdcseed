<?php

use \Illuminate\Database\Seeder;


class VisibiliteEventsSeeder extends Seeder
{
    public function run(){
        $data = [
            ["visibilite"=>"private","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],
            ["visibilite"=>"public","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],
            ["visibilite"=>"common","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()]
        ];
        DB::table('visibilite_events')->insert($data);
    }
}