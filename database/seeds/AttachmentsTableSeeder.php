<?php

use App\Attachment;
use Illuminate\Database\Seeder;

class AttachmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//<<<<<<< HEAD:database/seeds/AttachmentsTableSeeder.php
        factory(Attachment::class, 20)->create();

        factory(\App\Candidate::class, 5)->create()->each(function ($candidate) {
            $faker = \Faker\Factory::create();

            /**
             * Templates
             */
            $quantityTemplates = random_int(1, 4);
            for ($i = 0; $i < $quantityTemplates; $i++) {
                $candidate->templates()->create([
                    'name' => $faker->text(100),
                    'description' => $faker->realText(200),
                    'content_type' => 'text/html',
                    'content_value' => $faker->realText(200)
                ]);
            }

            /**
             * hire plans
             */
            $candidate->candidatePlans()->create([
                'plan_id' => random_int(1, 6),
                'hiring_start' => \Carbon\Carbon::now()
            ]);


            /**
             * Senders
             */
            $candidate->senders()->create([
                'nick_name' => $faker->name,
                'from' => $faker->email,
                'from_name' => $faker->name,
                'replay_to' => $faker->email,
                'address' => $faker->address,
                'city_id' => random_int(1, 100),
                'state_id' => random_int(1, 27),
                'country' => 'Brasil',
                'verified' => true,
                'replay_to_name' => $faker->name(),
                'zip' => $faker->numerify('###') . $faker->numerify('###') . "-" . $faker->numerify('###'),
            ]);

            /**
             * List e contacts, contacts is add in list
             */
            $candidate->listMarketing()->create([
                'name' => $faker->text(200),
                'recipient_count'=>50
            ])->each(function ($list) {
                $faker = \Faker\Factory::create();
                for ($i = 0; $i < 50; $i++) {
                    $list->recipient_count ++;
                    $list->contacts()->attach(
                        \App\ContactMarketing::create([
                            'email' => $faker->email,
                            'first_name' => $faker->firstName,
                            'last_name' => $faker->lastName,
                            'candidate_id' => $list->candidate_id
                        ])
                    );
                }
            });

        });

    }
}
