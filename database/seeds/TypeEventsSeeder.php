<?php

use Illuminate\Database\Seeder;

class TypeEventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["type"=>"Aniversário","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],
            ["type"=>"Política","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()],
            ["type"=>"Comum","created_at"=>\Carbon\Carbon::now(),'updated_at'=>\Carbon\Carbon::now()]
        ];
        DB::table('type_events')->insert($data);
    }
}
