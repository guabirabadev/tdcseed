<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTypesTableSeeder::class);
        // $this->call(RegionTableSeeder::class);
        // $this->call(StateTableSeeder::class);
        // $this->call(CityTableSeeder::class);
        // $this->call(PlanTableSeeder::class);
        $this->call(InteractionsTableSeeder::class);
        // $this->call(MessagesTableSeeder::class);
        // $this->call(MessagesToTableSeeder::class);
        // $this->call(AttachmentsTableSeeder::class);
        // $this->call(CandidateTableSeeder::class);
        // $this->call(ContactTableSeeder::class);
        // $this->call(AddressTableSeeder::class);
        // $this->call(UserTypesTableSeeder::class);
    }
}
