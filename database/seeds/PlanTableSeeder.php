<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>20.5,
                'name'=>'Plano de sms e email - simples',
                'renewal_frequency'=>30
            ],
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>40.5,
                'name'=>'Plano de sms e email - turbo',
                'renewal_frequency'=>30
            ],
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>80.5,
                'name'=>'Plano de sms e email - Mega',
                'renewal_frequency'=>30

            ],
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>5.5,
                'name'=>'Plano de sms e email - Semanal',
                'renewal_frequency'=>7

            ],
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>10.5,
                'name'=>'Plano de sms e email - Quinzenal',
                'renewal_frequency'=>15

            ],
            [
                'max_users' => random_int(20, 100),
                'max_electors' => random_int(2000, 10000),
                'sms_quantity' => random_int(2000, 10000),
                'emails_quantity'=> random_int(2000, 10000),
                'price'=>2.5,
                'name'=>'Plano de sms e email - diario',
                'renewal_frequency'=>1
            ]
        ];
        \Illuminate\Support\Facades\DB::table('plans')->insert($data);
    }
}
