<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'name' => 'User Common',
            'email' => 'user@user.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10)]);

        factory(\App\User::class)->create([
            'name' => 'Admin root',
            'email' => 'root@user.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10)]);
        // $users = [
        //     1 => [
        //         'name' => 'Admin',
        //         'email' => 'admin@elejase.com',
        //         'password' => '123456',
        //         'type' => 'super'
        //     ],
        //     2 => [
        //         'name' => 'Candidato',
        //         'email' => 'candidate@elejase.com',
        //         'password' => '123456',
        //         'type' => 'candidate'
        //     ],
        //     3 => [
        //         'name' => 'Voter',
        //         'email' => 'voter@elejase.com',
        //         'password' => '123456',
        //         'type' => 'voter'
        //     ],
        //     4 => [
        //         'name' => 'User',
        //         'email' => 'user@elejase.com',
        //         'password' => '123456',
        //         'type' => 'default'
        //     ]
        // ];

        // $a = 1;
        // foreach( $users as $user ) {
        //     $faker = Faker\Factory::create();

        //     if( $user['type'] != 'voter' ) {
        //         \App\Models\User::create([
        //             'name' => $user['name'],
        //             'email' => $user['email'],
        //             'password' => bcrypt($user['password']),
        //             'type' => $user['type'],
        //             'remember_token' => str_random(10)
        //         ]);
        //     }

        //     \App\Models\Contact::create([
        //         'user_id' => $a,
        //         'type' => $faker->phoneNumber,
        //         'contact' => $faker->safeEmail,
        //         'description' => $faker->text()
        //     ]);

        //     \App\Models\Address::create([
        //         'user_id' => $a,
        //         'street' => $faker->address,
        //         'number' => $faker->randomDigit,
        //         'district' => $faker->title,
        //         'city' => $faker->city,
        //         'state' => $faker->title,
        //         'complement' => $faker->title,
        //         'reference' => $faker->title
        //     ]);

        //     switch ( $user['type'] ):
        //         case 'voter':
        //             \App\Models\Voter::create([
        //                 'user_id' => 2, // for candidate
        //                 'nickname' => $user['name'],
        //                 'genre' => 1,
        //                 'birth' => date('Y-m-d'),
        //                 'observations' => $faker->text(300)
        //             ]);
        //             break;
        //         case 'candidate':
        //             \App\Models\Candidate::create([
        //                 'user_id' => $a,
        //                 'name_political' => $user['name'],
        //                 'membership' => 'PN ( Partido Nulo )',
        //                 'biograph' => $faker->text(300)
        //             ]);
        //             break;
        //         default:
        //             \App\Models\Profile::create([
        //                 'user_id' => $a,
        //                 'nickname' => $user['name'],
        //                 'genre' => 1,
        //                 'birth' => date('Y-m-d'),
        //                 'observations' => $faker->text(300)
        //             ]);
        //             break;
        //      endswitch;

        //     $a++;
        // }

    }
}
