<?php

use Illuminate\Database\Seeder;

class InteractionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Interactions::class, 15000)->create();
    }
}
