<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/regions.json");
        $regions = json_decode($json);
        foreach ($regions as $region){
            Region::create(array(
                'id' => $region->id,
                'name' => $region->name
            ));
        }
    }
}
