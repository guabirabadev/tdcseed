<?php

use App\Occupations;
use Illuminate\Database\Seeder;

class OccupationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Occupations::class)->create([
            'name' => 'ADMINISTRADOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ADMINISTRADOR DE EDIFICIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ADMINISTRADOR DE EXPLORACAO AGRICOLA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ADMINISTRADORES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ADVOGADO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ADVOGADO DIREITO DO TRABALHO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENCIADOR DE PROPAGANDA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE ADMINISTRATIVO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE DE COMPRAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE DE VENDAS DE SERVICOS AS EMPRESAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE DE VIAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE PUBLICITARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTE TECNICO DE VENDAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTES ADMINISTRATIVOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AGENTES DE ADMINISTRACAO DE EMPRESAS PUBLICAS E PRIVADAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ALMOXARIFE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE CARGOS E SALARIOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE COMERCIALIZACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE COMUNICACA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE IMPORTACAO E EXPORTACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE OCUPACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE ORGANIZACAO E METODOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE PESQUISA DE MERCADO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE RECURSOS HUMANOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE SEGUROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE SISTEMAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE SUPORTE DE SISTEMA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE MAO DE OBRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ANALISTA DE PRODUCAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ARQUITETO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ARQUIVISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ASSISTENTE ADMINISTRATIVO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ASSISTENTE DE PATRIMONIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ASSISTENTE DE VENDAS FINANCEIRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ASSISTENTE SOCIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ASSISTENTE DE ENFERMAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ATLETA PROFISSIONAL DE FUTEBOL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUDITOR CONTABIL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUDITOR GERAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE ALMOXARIFADO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE BIBLIOTECA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE CONTABILIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE ENFERMAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE ESCRITORIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE FARMACIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE IMPORTACAO E EXPORTACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE LABORATORIO DE ANALISES CLINICAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE LABORATORIO DE ANALISES FISICOQUIMICAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE PESSOAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE SEGUROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE SERVICOS JURIDICOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE CONTABILIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'AUXILIAR DE ESCRITORIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'BABA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'BARMAN',
        ]);

        factory(Occupations::class)->create([
            'name' => 'BIBLIOTECARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'BIOLOGISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'BIOQUIMICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CABELEREIRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CARTEIRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ADMINISTRATIVOS',
        ]);


        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ALMOXARIFADO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE CONTABILIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE CONTABILIDADE E FINANCAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE CONTAS A PAGAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE CONTROLE DE PATRIMONIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO DE CONTABILIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO DE CREDITO E COBRANCA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO ORCAMENTO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO PESSOAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO SERVICOS GERAIS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE ESCRITORIO TESOURARIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE RECEPCAO HOTEL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE SERVICO DE TRANSPORTE RODOVIARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CHEFE DE SERVICOS DE TELECOMUNICACOES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CIRURGIAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CIRURGIAO DENTISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CODIFICADOR DE DADOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'COMERCIANTE VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'COMISSARIO DE BORDO AERONAVES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'COMPRADOR COMERCIO ATACADISTA E VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CONDUTOR DE CAMINHAO BASCULANTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CONDUTORES DE AUTOMOVEIS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CONSULTOR JURIDICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CONTADOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CONTRAMESTRE DE EMBARCACAO',
        ]);


        factory(Occupations::class)->create([
            'name' => 'CONTRAMESTRE INDUSTRIA TEXTIL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'COORDENADOR DE ENSINO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CORESPONDENTE COMERCIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'COZINHEIRO CHEFE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'CRONOANALISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DATILOGRAFO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DEMONSTRADOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DESENHISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DESENHISTA PROJETISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DESENHISTA TECNICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DESPACHANTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIAGRAMADOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA DE COMERCIO VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA DE COMUNICACOES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA DE CONSTRUCAO CIVIL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA DE PRESTACAO DE SERVICOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA DE SERVICOS CLINICOS E HOSPITALARES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA FINANCEIRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESA MANUFATUREIRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE ESTABELECIMENTO DE ENSINO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE ESTABELECIMENTO DE ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'DIRETOR DE EMPRESAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ECONOMISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ECONOMISTA MERCADOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ECONOMISTA PROGRAMACAO ECONOMICO FINANCEIRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ECONOMISTAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'EDITOR DE LIVROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENCARREGADO DE DIGITACAO E OPERACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENFERMEIRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENFERMEIRO DO TRABALHO',
        ]);


        factory(Occupations::class)->create([
            'name' => 'ENFERMEIROS',
        ]);


        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO AERONAUTICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO AGRONOMO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO CIVIL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE CONTROLE DE QUALIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE MANUTENÇÃP ELETRICIDADE ELETRONICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE MINAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE ORGANIZACAO E METODOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE SEGURANCA NO TRABALHO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE TELECOMUNICACOES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO DE TRAFEGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO ELETRICISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO ELETRONICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO MECANICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIRO QUIMICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ENGENHEIROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ESCRITUARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ESCRIVAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ESTATISTICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ESTETICISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FARMACEUTICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FONOAUDIOLOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FUNCIONARIO PUBLICO ESTUDAL SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FUNCIONARIO PUBLICO FEDERAL SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FUNCIONARIO PUBLICO MUNICIPAL SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'FUNCIONARIO PUBLICO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GEOLOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE ADMINISTRATIVO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE COMERCIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE BANCO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE BAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE COMPRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE EMPRESAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE HOTEL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE INFORMATICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE LOJA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE MARKETING',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE OPERACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PESQUISA E DESENVOLVIMENTO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PESSOAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PLANEJAMENTO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE POSTAL E TELECOMUNICACOES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PRODUCAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PRODUCAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE PROPAGANDA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE RESTAURANTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE RH',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE TRANSPORTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE DE VENDAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE EXECUTIVO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE FINANCEIRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'GERENTE OPERACIONAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'INSPETOR DE PRODUCAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'INSPETOR DE QUALIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'INSPETOR DE SERVICOS DE TRANSPORTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'INSPETOR TECNICO DE VENDAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'INSPETOR DE APRENDIZAGEM E TREINAMENTO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'JORNALISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'LABORATORISTA ANALISES CLINICAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'LABORATORISTA INDUSTRIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'LOCUTOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MAITRE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO ANESTESISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO CARDIOLOGISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO ORTOPEDISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO DO TRABALHO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO GINECOLOGISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO ORTOPEDISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO PEDIATRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO PSIQUIATRA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MEDICO VETERINARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MESTRE DE CONSTRUCAO CIVIL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MESTRE INDUSTRIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MILITAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTOCICLISTA TRANSPORTE DE MERCADORIAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTORISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTORISTA DE CAMINHAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTORISTA DE FURGAO OU VEICULO SIMILAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTORISTA DE ONIBUS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MOTORISTA DE TAXI',
        ]);

        factory(Occupations::class)->create([
            'name' => 'MUSICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'NUTRICIONISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE CAMERA DE TELEVISAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE EQUIPAMENTOS DE ENTRADA DE DADOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE ESTACAO DE RADIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE MAQUINAS E VEICULOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE MICRO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE PRODUTOS FINANCEIROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE RAIOS X',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OPERADOR DE TELEMARKETING',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ORIENTADOR EDICACIONAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'OURIVES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PEDAGOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PILOTO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PINTOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PRODUTOR DE RADIO E TELEVISAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE 1A A 4A SERIE ENSINO DE 1O GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ADMINISTRACAO ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ALUNOS COM NESCESSIDADES ESPECIAIS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE BIOLOGIA ENSINO DE 2O GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE CIENCIAS NATURAIS ENSINO DE 1O GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE COMUNICACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE CONTABILIDADE ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE DIDATICA ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE DIREITO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ECONOMIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE EDUCACAO FISICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ENFERMAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ENSINO PRE ESCOLAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ESTATISTICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ESTUDOS SOCIAIS ENSINO DE 1O GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE FISICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE FISIOTERAPIA ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE HISTORIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE INGLES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE LINGUA PORTUGUESA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE LINGUAS ESTRANGEIRAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE MATEMATICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE ORIENTACAO EDUCACIONAL SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE PEDAGOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE PORTUGUES E LITERATURA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE PRATICA DE ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE PRATICA DE PSICOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE QUIMICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE SOCIOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSOR DE TECNOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE BIOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE CIENCIAS HUMANAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE ENSINO DO 2O GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE ENSINO DE PRIMEIRO GRAU',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE ENSINO ESPECIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE ENSINO PRE ESCOLAR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE ENSINO SUPERIOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE PEDAGOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROFESSORES DE GEOGRAFIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROGRAMADOR DE COMPUTADOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROPAGANDISTA DE PRODUTOS DE LABORATORIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PROPAGANDISTA DE PSICOLOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'PSICOLOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'QUIMICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'QUIMICO ANALISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'QUIMICO INDUSTRIAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'RECEPCIONISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'REDATOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'RELACOES PUBLICAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SECRETARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SECRETARIO BILINGUE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SECRETARIO EXECUTIVO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SERVENTUARIOS DA JUSTICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SOCIOLOGO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISOR DE COMPRAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISOR DE VENDAS COMERCIO ATACADISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISOR DE VENDAS COMERCIO VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISOR EDUCACIONAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISORES DE COMPRAS E COMPRADORES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'SUPERVISORES DE VENDAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO AGRICOLA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO AGROPECUARIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE ADMINISTRACAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE CONTABILIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE ENFERMAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE LABORATORIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE MANUTENCAO ELETRICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE MANUTENCAO ELETRONICA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE OBRAS CIVIS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE PLANEJAMENTO DE PRODUCAO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE SEGURANCA DO TRABALHO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE SEGUROS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE TELECOMUNICACOES',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO ELETRONICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO MECANICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO MECANICO MAQUINAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO METALURGICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO QUIMICO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE BIOLOGIA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE ELETRICIDADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE ENFERMAGEM',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TECNICO DE OBRAS CIVIS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TERAPEUTA OCUPACIONAL',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TOPOGRAFO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TRABALHADORES DAS PROFISSOES CIENTIFICAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TRABALHADORES DE COMERCIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TRABALHADORES DE SERVICOS ADMINISTRATIVOS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TRABALHADORES DE SERVICOS DE CONTABILDIADE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'TRABALHADORES DE SERVICOS DE TURISMO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR A DOMICILIO',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR A AMBULANTE',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR DE COMERCIO ATACADISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR DE COMERCIO VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR DE PRACISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR DE COMERCIO ATACADISTA E VAREJISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDORES PRACISTAS',
        ]);

        factory(Occupations::class)->create([
            'name' => 'VENDEDOR',
        ]);

        factory(Occupations::class)->create([
            'name' => 'ZOOTECNISTA',
        ]);

        factory(Occupations::class)->create([
            'name' => 'WEBDESIGNER',
        ]);

    }
}
