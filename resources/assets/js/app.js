
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


const LoginComponent = require('./components/Login.vue');
const SignupComponent = require('./components/Signup.vue');

const AppComponent = require('./components/App.vue');
const DashboardComponent = require('./components/Dashboard.vue');
//components contributors
const ContributorsComponent = require('./components/contributors/Contributors.vue');
const ContributorsListComponent = require('./components/contributors/List.vue');
const ContributorsCreateComponent = require('./components/contributors/Create.vue');

// components search
const SearchComponent = require('./components/Search.vue');

const router = new VueRouter({
    routes: [
        { path: '/', name: 'geral', component: AppComponent, children:[
            {path: '/dashboard', name: 'dashboard', component: DashboardComponent},
            {path: '/contributors', name: 'contributors', component: ContributorsComponent, children:[
                {path: '/contributors/list', name: 'contributors.list', component: ContributorsListComponent},
            ]},
            { path: '/search', name: 'voters.search', component: SearchComponent }
        ]},
        { path: '/login', name: 'auth.login', component: LoginComponent },
        { path: '/sign_up', name: 'auth.sign_up', component: SignupComponent }
    ]
});

const app = new Vue({
    router
}).$mount('#app');