<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('layouts.spa');
});

Route::get('/teste', function () {

});

Route::get('/insert', function () {
    return response()->json(['msg' => 'ok']);
})->middleware('can:insert');
Route::get('/update', function () {
    return response()->json(['msg' => 'ok']);
})->middleware('can:update');
Route::get('register/confirm/{toke}', 'UserController@confirmEmail');


//Route::get('/home', 'HomeController@index');

Route::get('images/{folder}/{folder2}/{filename}',
    function ($folder, $folder2, $filename) {
        $ds = DIRECTORY_SEPARATOR;
        $file_path = $folder . $ds . $folder2 . $ds . $filename;
        $path = Config::get('filesystems.disks.uploads.root') . $file_path;
        $key = "images.{$folder}.{$folder2}.{$filename}";

        if( !Cache::has( $key ) ) {
            $content = File::get($path);
            Cache::store('file')->put($key, $content, 240);
        } else {
            $content = Cache::get( $key );
        }
        $response = Response::make($content);
        $contentType = Storage::drive('uploads')->mimeType($file_path);
        $response->header('Content-Type', $contentType);
        return $response;
    });

Auth::routes();