<?php

use Illuminate\Http\Request;
use Thujohn\Twitter\Facades\Twitter;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['cors']], function(){
    Route::post('auth', 'Auth\AuthController@authenticate');
    Route::get('auth/me', 'Auth\AuthController@getAuthenticatedUser');
});

Route::group(['middleware' => ['cors']], function(){


Route::group(['middleware' => ['jwt.auth', 'cors']], function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    })->middleware('jwt.auth');

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/



/*
|--------------------------------------------------------------------------
| users Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['prefix' => 'users', 'middleware' => ['cors']], function() { 
    Route::resource('', 'UserController@index', [
        'except' => []
        ]);

    Route::get('hierarchy/{id}', 'UserHierarchyController@check');

    Route::resource('hierarchy', 'UserHierarchyController', [
        'except' => ['create', 'edit']
        ]);
});

/*
|--------------------------------------------------------------------------
| votes Routes
|--------------------------------------------------------------------------
|
*/
Route::group([
    'prefix' => 'votes'
    ], function() {
        Route::resource('', 'VoteController', [
            'except' => [ 'create', 'edit' ]
            ]);

        Route::get('types', 'VoteController@types');
    });

/*
|--------------------------------------------------------------------------
| files Routes
|--------------------------------------------------------------------------
|
*/
Route::group([
    'prefix' => 'files'
    ], function() {
        Route::resource('', 'FilesController');
    });

/*
|--------------------------------------------------------------------------
| others routes
|--------------------------------------------------------------------------
|
*/
Route::resource('zones', 'ZonesController', [
    'except' => [ 'create', 'edit' ]
    ]);

Route::group([
    'prefix' => 'educations'
    ], function() {
        Route::get('list', function() {
            return response()->json(config('system.educations'), 201);
        });
        Route::resource('', 'EducationController', [
            'except' => [ 'create', 'edit' ]
            ]);
    });

Route::group([
    'prefix' => 'voters'
    ], function() {
        Route::resource('', 'VoterController', [
            'except' => [  ]
            ]);

        Route::post('search', 'SearchController@voters');
    });

Route::resource('candidates', 'CandidateController', [
    'except' => []
    ]);
Route::resource('contacts', 'ContactController');

Route::resource('addresses', 'AddressController', [
    'except' => ['create', 'edit']
    ]);


Route::resource('contacts', 'ContactController', [
    'except' => ['create', 'edit']
    ]);


Route::resource('usertype', 'UserTypesController', [
    'except' => ['create', 'edit']
    ]);

Route::resource('plan', 'PlanController', [
    'except' => ['create', 'edit']
    ]);
Route::resource('plan/consume', 'ConsumePlanController', [
    'except' => ['create', 'edit']
    ]);

Route::resource('plan/hire', 'HirePlansController', [
    'except' => ['create', 'edit']
    ]);


Route::resource('occupations', 'OccupationsController');
/**
 * Teste de permissão
 */
Route::get('/insert', function () {
    return response()->json(['msg' => 'ok']);
})->middleware('jwt.auth', 'can:insert');

/**.
 * Crud de Permissão
 */

Route::get('permission', 'Permissions\PermissionController@all')->middleware('jwt.auth', 'can:admin');
Route::post('permission', 'Permissions\PermissionController@insert')->middleware('jwt.auth', 'can:admin');
Route::put('permission', 'Permissions\PermissionController@update')->middleware('jwt.auth', 'can:admin');
Route::delete('permission/{id}', 'Permissions\PermissionController@delete')->middleware('jwt.auth', 'can:admin');

/**.
 * Crud de Role
 */
Route::get('role', 'Permissions\RoleController@all')->middleware('jwt.auth', 'can:admin');
Route::post('role', 'Permissions\RoleController@insert')->middleware('jwt.auth', 'can:admin');
Route::put('role', 'Permissions\RoleController@update')->middleware('jwt.auth', 'can:admin');
Route::delete('role/{id}', 'Permissions\RoleController@delete')->middleware('jwt.auth', 'can:admin');

/**
 * Crud Permisson Role
 */
Route::get('permission/role', 'Permissions\PermissionRoleController@all')->middleware('jwt.auth', 'can:admin');
Route::post('permission/role', 'Permissions\PermissionRoleController@insert')->middleware('jwt.auth', 'can:admin');
Route::put('permission/role', 'Permissions\PermissionRoleController@update')->middleware('jwt.auth', 'can:admin');
Route::delete('permission/role/{id}', 'Permissions\PermissionRoleController@delete')->middleware('jwt.auth', 'can:admin');
/**
 * Crud Role User
 */
Route::get('role/user', 'Permissions\RoleUserController@all')->middleware('jwt.auth', 'can:admin');
Route::post('role/user', 'Permissions\RoleUserController@insert')->middleware('jwt.auth', 'can:admin');
Route::put('role/user', 'Permissions\RoleUserController@update')->middleware('jwt.auth', 'can:admin');
Route::delete('role/user/{id}', 'Permissions\RoleUserController@delete')->middleware('jwt.auth', 'can:admin');
/**
 * E-mail Marketing
 */

Route::get('marketing/campaign/send/{id}','CampaignMarketingController@send');
Route::post('marketing/list/contact','ListMarketingController@storeContact');
Route::delete('marketing/list/contact/{list}/{contact}','ListMarketingController@destroyContact');
Route::get('marketing/candidate/list/{candidate}','ListMarketingController@listForCandidate');
Route::resource('marketing/campaign','CampaignMarketingController');
Route::resource('marketing/lists','ListMarketingController');
Route::resource('marketing/contacts','ContactsMarketingController');
Route::resource('marketing/senders','SenderController');
Route::resource('marketing/email/template','TemplateEmailMarketingController');


Route::resource('regions', 'RegionController');
Route::resource('states', 'StateController');
Route::resource('cities', 'CityController');



//Route::resource('facebook', 'FacebookController');

//Social media in twitter
Route::group(['prefix' => 'twitter', 'as'=>'twitter.'], function (){

    Route::get('tweet', ['as' => 'tweet' ,'uses' => 'TweetController@instantaneoPost']);
    Route::get('getTimeline', ['as' => 'getTimeline' ,'uses' => 'TweetController@getUserTimeline']);
    Route::get('getMentionsTimeline', ['as' => 'getMentionsTimeline' ,'uses' => 'TweetController@getMentionsTimeline']);
    Route::get('getUsetTwitter', ['as' => 'getUsetTwitter' ,'uses' => 'TweetController@getUsetTwitter']);
    Route::get('loginWithTwitter', ['as' => 'loginWithTwitter' ,'uses' => 'TweetController@loginWithTwitter']);
});

//Social media in twitter
Route::group(['prefix' => 'instagram', 'as'=>'instagram.'], function (){

    Route::get('index', ['as' => 'index' ,'uses' => 'InstagramController@index']);
    Route::get('login', ['as' => 'login' ,'uses' => 'InstagramController@login']);
    Route::post('post_image', ['as' => 'post_image' ,'uses' => 'InstagramController@post_image']);
    Route::post('post_album', ['as' => 'post_album' ,'uses' => 'InstagramController@post_album']);
    Route::post('post_video', ['as' => 'post_video' ,'uses' => 'InstagramController@post_video']);
    Route::post('statistics', ['as' => 'statistics' ,'uses' => 'InstagramController@statistics']);

});
Route::group(['prefix' => 'facebook', 'as'=>'facebook.'], function (){

    Route::get('index', ['as' => 'index' ,'uses' => 'FacebookController@index']);
    Route::get('login', ['as' => 'login' ,'uses' => 'FacebookController@login']);
    Route::get('callback', ['as' => 'callback' ,'uses' => 'FacebookController@callback']);
    Route::get('getUser', ['as' => 'getUser' ,'uses' => 'FacebookController@getUser']);
    Route::get('getAccessToken', ['as' => 'getAccessToken' ,'uses' => 'FacebookController@getAccessToken']);
    Route::post('postPhoto', ['as' => 'postPhoto' ,'uses' => 'FacebookController@postPhoto']);

});

//Hub Digital Marketing
Route::group(['prefix' => 'hub', 'as'=>'hub.'], function (){

    //assets
    Route::get('bot', ['as' => 'bot.index' ,'uses' => 'BotController@index']);


    Route::get('assets', ['as' => 'assets.index' ,'uses' => 'AssetsController@index']);
    Route::get('assets/create', ['as' => 'assets.create' ,'uses' => 'AssetsController@create']);
    Route::get('assets/edit/{id}', ['as' => 'assets.edit' ,'uses' => 'AssetsController@create']);
    Route::post('assets/store', ['as' => 'assets.store' ,'uses' => 'AssetsController@store']);
    Route::post('assets/update/{id}', ['as' => 'assets.update' ,'uses' => 'AssetsController@update']);
    Route::get('assets/destroy/{id}', ['as' => 'assets.destroy' ,'uses' => 'AssetsController@destroy']);
    Route::get('assets/show/{id}', ['as' => 'assets.show' ,'uses' => 'AssetsController@show']);

    //campaingns
    Route::get('campaingn', ['as' => 'campaingn.index' ,'uses' => 'CampaingnController@index']);
    Route::get('campaingn/create', ['as' => 'campaingn.create' ,'uses' => 'CampaingnController@create']);
    Route::get('campaingn/edit/{id}', ['as' => 'campaingn.edit' ,'uses' => 'CampaingnController@create']);
    Route::post('campaingn/store', ['as' => 'campaingn.store' ,'uses' => 'CampaingnController@store']);
    Route::post('campaingn/update/{id}', ['as' => 'campaingn.update' ,'uses' => 'CampaingnController@update']);
    Route::get('campaingn/destroy/{id}', ['as' => 'campaingn.destroy' ,'uses' => 'CampaingnController@destroy']);
    Route::get('campaingn/show/{id}', ['as' => 'campaingn.show' ,'uses' => 'CampaingnController@show']);

    //companies
    Route::get('companies', ['as' => 'companies.index' ,'uses' => 'CompaniesController@index']);
    Route::get('companies/create', ['as' => 'companies.create' ,'uses' => 'CompaniesController@create']);
    Route::get('companies/edit/{id}', ['as' => 'companies.edit' ,'uses' => 'CompaniesController@create']);
    Route::post('companies/store', ['as' => 'companies.store' ,'uses' => 'CompaniesController@store']);
    Route::post('companies/update/{id}', ['as' => 'companies.update' ,'uses' => 'CompaniesController@update']);
    Route::get('companies/destroy/{id}', ['as' => 'companies.destroy' ,'uses' => 'CompaniesController@destroy']);
    Route::get('companies/show/{id}', ['as' => 'companies.show' ,'uses' => 'CompaniesController@show']);

    //contacts
    Route::get('contacts', ['as' => 'contacts.index' ,'uses' => 'ContactMkController@index']);
    Route::get('contacts/create', ['as' => 'contacts.create' ,'uses' => 'ContactMkController@create']);
    Route::get('contacts/edit/{id}', ['as' => 'contacts.edit' ,'uses' => 'ContactMkController@create']);
    Route::post('contacts/store', ['as' => 'contacts.store' ,'uses' => 'ContactMkController@store']);
    Route::post('contacts/update/{id}', ['as' => 'contacts.update' ,'uses' => 'ContactMkController@update']);
    Route::get('contacts/destroy/{id}', ['as' => 'contacts.destroy' ,'uses' => 'ContactMkController@destroy']);
    Route::get('contacts/show/{id}', ['as' => 'contacts.show' ,'uses' => 'ContactMkController@show']);

    //Landing_pages
    Route::get('landing', ['as' => 'landing.index' ,'uses' => 'LandingPageController@index']);
    Route::get('landing/create', ['as' => 'landing.create' ,'uses' => 'LandingPageController@create']);
    Route::get('landing/edit/{id}', ['as' => 'landing.edit' ,'uses' => 'LandingPageController@create']);
    Route::post('landing/store', ['as' => 'landing.store' ,'uses' => 'LandingPageController@store']);
    Route::post('landing/update/{id}', ['as' => 'landing.update' ,'uses' => 'LandingPageController@update']);
    Route::get('landing/destroy/{id}', ['as' => 'landing.destroy' ,'uses' => 'LandingPageController@destroy']);
    Route::get('landing/show/{id}', ['as' => 'landing.show' ,'uses' => 'LandingPageController@show']);

    //Segments
    Route::get('segments', ['as' => 'segments.index' ,'uses' => 'SegmentsController@index']);
    Route::get('segments/create', ['as' => 'segments.create' ,'uses' => 'SegmentsController@create']);
    Route::get('segments/edit/{id}', ['as' => 'segments.edit' ,'uses' => 'SegmentsController@create']);
    Route::post('segments/store', ['as' => 'segments.store' ,'uses' => 'SegmentsController@store']);
    Route::post('segments/update/{id}', ['as' => 'segments.update' ,'uses' => 'SegmentsController@update']);
    Route::get('segments/destroy/{id}', ['as' => 'segments.destroy' ,'uses' => 'SegmentsController@destroy']);
    Route::get('segments/show/{id}', ['as' => 'segments.show' ,'uses' => 'SegmentsController@show']);

    //Stages
    Route::get('stages', ['as' => 'stages.index' ,'uses' => 'StageController@index']);
    Route::get('stages/create', ['as' => 'stages.create' ,'uses' => 'StageController@create']);
    Route::get('stages/edit/{id}', ['as' => 'stages.edit' ,'uses' => 'StageController@create']);
    Route::post('stages/store', ['as' => 'stages.store' ,'uses' => 'StageController@store']);
    Route::post('stages/update/{id}', ['as' => 'stages.update' ,'uses' => 'StageController@update']);
    Route::get('stages/destroy/{id}', ['as' => 'stages.destroy' ,'uses' => 'StageController@destroy']);
    Route::get('stages/show/{id}', ['as' => 'stages.show' ,'uses' => 'StageController@show']);

    //Analistics -- dei um time para testar algumas features
    Route::get('stages', ['as' => 'stages.index' ,'uses' => 'StageController@index']);
    Route::get('stages/create', ['as' => 'stages.create' ,'uses' => 'StageController@create']);
    Route::get('stages/edit/{id}', ['as' => 'stages.edit' ,'uses' => 'StageController@create']);
    Route::post('stages/store', ['as' => 'stages.store' ,'uses' => 'StageController@store']);
    Route::post('stages/update/{id}', ['as' => 'stages.update' ,'uses' => 'StageController@update']);
    Route::get('stages/destroy/{id}', ['as' => 'stages.destroy' ,'uses' => 'StageController@destroy']);
    Route::get('stages/show/{id}', ['as' => 'stages.show' ,'uses' => 'StageController@show']);
});
//messages - #19 - internall communications

Route::resource('messages', 'MessagesController');
});
});