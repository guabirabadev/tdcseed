<?php

namespace App\Presenters;

use App\Transformers\InteractionsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InteractionsPresenter
 *
 * @package namespace App\Presenters;
 */
class InteractionsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new InteractionsTransformer();
    }
}
