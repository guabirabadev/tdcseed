<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function boot()
    {
        //
    }


    public function register()
    {
        //dis para o provider instanciar o repositoryEloquente (se precisar mudar de ORM algum dia basta midar
        // a segunda linha do metodo para OccupationsRepositoryDoctrine pro exemplo )
        $this->app->bind(
            'App\Repositories\OccupationsRepository',
            'App\Repositories\OccupationsRepositoryEloquent'
        );

        $this->app->bind(

            'App\Repositories\TwitterRepository',
            'App\Repositories\TwitterRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\TweetRepository',
            'App\Repositories\TweetRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\TweetScheduleRepository',
            'App\Repositories\TweetScheduleRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\AssetRepository',
            'App\Repositories\AssetRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\CampaignRepository',
            'App\Repositories\CampaignRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\ChanelRepository',
            'App\Repositories\ChanelRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\CompaniesRepository',
            'App\Repositories\CompaniesRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\ContactRepository',
            'App\Repositories\ContactRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\LandingPageRepository',
            'App\Repositories\LandingPageRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\SegmentsRepository',
            'App\Repositories\SegmentsRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\StageRepository',
            'App\Repositories\StageRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\TemplateRepository',
            'App\Repositories\TemplateRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\InstagramRepository',
            'App\Repositories\InstagramRepositoryEloquent'

        );

        $this->app->bind(
            'App\Repositories\MessagesToRepository',
            'App\Repositories\MessagesToRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\AttachmentRepository',
            'App\Repositories\AttachmentRepositoryEloquent'

        );
    }
}
