<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sender extends Model
{
    protected $fillable = [
        'nick_name','from','replay_to','address','address_2','city_id',
        'state_id','candidate_id','country','verified','locked'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function city(){
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function states(){
        return $this->belongsTo(State::class,'state_id');
    }
}
