<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignMarketing extends Model
{
    protected $fillable = ['name', 'sender_id', 'template_email_marketing_id',
        'list_marketing_id', 'candidate_id', 'scheduling'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function templateEmailMarketing()
    {
        return $this->belongsTo(TemplateEmailMarketing::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function sender(){
        return $this->belongsTo(Sender::class);
    }

    protected function listContacts(){
        return $this->belongsTo(ListMarketing::class,'list_marketing_id');
    }

}
