<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TweetScheduleRepository
 * @package namespace App\Repositories;
 */
interface TweetScheduleRepository extends RepositoryInterface
{
    //
}
