<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InteractionsRepository
 * @package namespace App\Repositories;
 */
interface InteractionsRepository extends RepositoryInterface
{
    //
}
