<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InstagramRepository
 * @package namespace App\Repositories;
 */
interface InstagramRepository extends RepositoryInterface
{
    //
}
