<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Messages_toRepository;
use App\MessagesTo;
use App\Validators\MessagesToValidator;

/**
 * Class MessagesToRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MessagesToRepositoryEloquent extends BaseRepository implements MessagesToRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MessagesTo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
