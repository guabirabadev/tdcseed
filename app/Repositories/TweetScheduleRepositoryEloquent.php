<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TweetScheduleRepository;
use App\TweetSchedule;
use App\Validators\TweetScheduleValidator;

/**
 * Class TweetScheduleRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TweetScheduleRepositoryEloquent extends BaseRepository implements TweetScheduleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TweetSchedule::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
