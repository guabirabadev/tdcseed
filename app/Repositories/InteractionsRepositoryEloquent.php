<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\interactionsRepository;
use App\Models\Interactions;
use App\Validators\InteractionsValidator;

/**
 * Class InteractionsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class InteractionsRepositoryEloquent extends BaseRepository implements InteractionsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Interactions::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return InteractionsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
