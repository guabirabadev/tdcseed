<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TwitterRepository
 * @package namespace App\Repositories;
 */
interface TwitterRepository extends RepositoryInterface
{
    //
}
