<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TwitterRepository;
use App\Twitter;
use App\Validators\TwitterValidator;

/**
 * Class TwitterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TwitterRepositoryEloquent extends BaseRepository implements TwitterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Twitter::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
