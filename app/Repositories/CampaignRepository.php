<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CampaignRepository
 * @package namespace App\Repositories;
 */
interface CampaignRepository extends RepositoryInterface
{
    //
}
