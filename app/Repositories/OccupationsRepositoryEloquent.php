<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OccupationsRepository;
use App\Occupations;
use App\Validators\OccupationsValidator;

/**
 * Class OccupationsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OccupationsRepositoryEloquent extends BaseRepository implements OccupationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Occupations::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
