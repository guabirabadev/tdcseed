<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessagesToRepository
 * @package namespace App\Repositories;
 */
interface MessagesToRepository extends RepositoryInterface
{
    //
}
