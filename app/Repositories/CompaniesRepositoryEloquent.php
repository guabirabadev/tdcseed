<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CompaniesRepository;
use App\Models\Companies;
use App\Validators\CompaniesValidator;

/**
 * Class CompaniesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CompaniesRepositoryEloquent extends BaseRepository implements CompaniesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Companies::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
