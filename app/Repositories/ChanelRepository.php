<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChanelRepository
 * @package namespace App\Repositories;
 */
interface ChanelRepository extends RepositoryInterface
{
    //
}
