<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\InstagramRepository;
use App\Instagram;
use App\Validators\InstagramValidator;

/**
 * Class InstagramRepositoryEloquent
 * @package namespace App\Repositories;
 */
class InstagramRepositoryEloquent extends BaseRepository implements InstagramRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Instagram::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
