<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ChanelRepository;
use App\Models\Chanel;
use App\Validators\ChanelValidator;

/**
 * Class ChanelRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ChanelRepositoryEloquent extends BaseRepository implements ChanelRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Chanel::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
