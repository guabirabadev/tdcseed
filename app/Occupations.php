<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupations extends Model
{
    protected $fillable = [
        'name'
    ];
}
