<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMarketing extends Model
{
    protected $table = "contact_marketing";
    protected $fillable = ['email','first_name','last_name','custom_fields','candidate_id'];
}
