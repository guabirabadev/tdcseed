<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Plan extends Model
{
    protected $table = 'plans';
    protected $fillable = ['max_users', 'max_electors', 'sms_quantity', 'emails_quantity', 'price','renewal_frequency','name'];

    public function candidates(){
        return $this->belongsToMany(Candidate::class);
    }
}