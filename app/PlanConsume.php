<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanConsume extends Model
{
    public function consumes(){
        return $this->hasMany(Consume::class);
    }
}
