<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $fillable = ['id', 'name', 'initials', 'region_id'];

    function region(){
        return $this->hasOne('App\Region');
    }
}
