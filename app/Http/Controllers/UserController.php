<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Mail\ConfirmationEmail;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Metodo para mostrar todos os registros.
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Cria uma nova instância de usuário após um registro válido.
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Metodo para criar um novo registro.
     */
    public function store(UserRequest $request)
    {
        event(new Registered($voter = $this->create($request->all())));
        Mail::to($voter->email)->send(new ConfirmationEmail($voter));
        return response()->json($voter, 201);
    }

    /**
     * Metodo para mostrar um registro selecionando um ID especifico.
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Metodo para atualizar registro selecionando um ID especifico.
     */
    public function update(UserRequest $request, User $user)
    {
        $user->fill($request->all());
        $user->save();
        return response()->json($user);
    }

    /**
     * Metodo para remover registro selecionando um ID especifico.
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([], 204);
    }

    public function confirmEmail($token){
        User::whereToken($token)->firstOrFail()->hasVerified();
        return "E-mail confirmado com sucesso";
    }
}
