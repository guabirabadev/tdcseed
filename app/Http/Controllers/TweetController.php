<?php

namespace App\Http\Controllers;

use App\Repositories\TweetRepository;
use App\Repositories\TweetScheduleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Thujohn\Twitter\Facades\Twitter;

class TweetController extends Controller
{
    /**
     * @var TweetRepository
     */
    private $repository;
    /**
     * @var TweetScheduleRepository
     */
    private $tweetScheduleRepository;

    public function __construct(TweetRepository $repository,
                                TweetScheduleRepository $tweetScheduleRepository)
    {

        $this->repository = $repository;
        $this->tweetScheduleRepository = $tweetScheduleRepository;
    }

    public function index()
    {
        //Auth::user()->id; Quando estiver usando usuarios registrados no sistema
        $tweets = $this->repository->paginate(10);
        return $tweets;
    }

    public function store(Request $request)
    {
        $tweet = $request->all();
        $this->repository->create($tweet);
        return response()->json($tweet, 201);
    }

    public function show($id)
    {
        $tweetSpecific = $this->repository->find($id);
        return $tweetSpecific;
    }

    public function update(Request $request, $id)
    {
        $toUpdate = $request->all();
        $this->repository->update($toUpdate, $id);
        return response()->json($toUpdate);
    }

    public function destroy($id)
    {
        $destroy = $this->repository->delete($id);
        return response()->json($destroy, 204);
    }

    public function schedule()
    {
        //implementar via AJAX
    }

    /***
        get data user 
    */
    public function getUsetTwitter()
    {
        $credentials = Twitter::getCredentials([
            'include_email' => 'true',
        ]);

        return response()->json($credentials, 201);
    }
    /**
     * @param $simpleStatusUpdate
     * @return JSOn to confirm success to update status on twitter
     * 
     *instantaneoPost post on twitter
     */
    public function instantaneoPost($simpleStatusUpdate)
    {
       return Twitter::postTweet(array('status' => 'simpleStatusUpdate', 'format' => 'json'));
    }


    /**
     * @param $simpleStatusUpdate
     * @return JSOn to confirm success to update status on twitter
     * 
     *get user timeline
     */
    public function getuserTimeline($userTwitterId)
    {
        return Twitter::getUserTimeline(['screen_name' => '$userTwitterId', 'count' => 20, 'format' => 'json']);
    }

    /**
     * @param $simpleStatusUpdate
     * @return JSOn to confirm success to update status on twitter
     * 
     * get mentions marks on twitter
     */
    public function getMentionsTimeline($twitterUserId)
    {
        return Twitter::getMentionsTimeline(['count' => 20, 'format' => 'json']);
    }

    public function uploadMedia(Request $request, $twitterUserId)
    {
        $uploaded_media = Twitter::uploadMedia(['media' => File::get(public_path('filename.jpg'))]);
        return Twitter::postTweet(['status' => 'Laravel is beautiful', 'media_ids' => $uploaded_media->media_id_string]);
<<<<<<< HEAD
=======
    }

    public function loginWithTwitter(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $sign_in_twitter = true;
        $force_login = false;
        
        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('twitter.callback'));

        if (isset($token['oauth_token_secret']))
        {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return Redirect::to($url);
        }
    }

    public function twitterCallBack()
    {
        if (Session::has('oauth_request_token'))
        {
            $request_token = [
                'token'  => Session::get('oauth_request_token'),
                'secret' => Session::get('oauth_request_token_secret'),
            ];

            Twitter::reconfig($request_token);

            $oauth_verifier = false;

            if (Input::has('oauth_verifier'))
            {
                $oauth_verifier = Input::get('oauth_verifier');
            }

            // getAccessToken() will reset the token for you
            $token = Twitter::getAccessToken($oauth_verifier);

            if (!isset($token['oauth_token_secret']))
            {
                return Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
            }

            $credentials = Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error))
            {
                Session::put('access_token', $token);

                return Redirect::to('/')->with('flash_notice', 'Você adicionou a sua conta do Twitter, Parabems!');
            }

            return Redirect::route('twitter.error')->with('flash_error', 'Não foi possivel adiciona a sua conta do twitter!');
        }
>>>>>>> origin/dev#31-facebook
    }

    /*
     * PS:
     * other methods dependencies of Social Media Mangaer interface
     *
     * */
}
