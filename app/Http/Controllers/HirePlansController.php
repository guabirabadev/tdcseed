<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\CandidatePlan;
use App\HirePlans;
use Illuminate\Http\Request;

class HirePlansController extends Controller
{
    /**
     * lista todos os planos contratados
     * @return \Illuminate\Http\JsonResponse3
     */
    public function index()
    {
        return response()->json(['error' => 0, "msg" => "", "data" => CandidatePlan::all()], 200);
    }

    /**
     * Contrata um novo plano
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'plan_id' => 'required|exists:plans,id',
            'candidate_id' => 'required|exists:candidates,id',
            'automatic_renew' => 'boolean',
            'hiring_start' => 'required|date',
            'hiring_end' => 'date',
        ];
        $messages = [
            'plan_id.required' => "O plano é requerido.",
            'plan_id.exists' => "O plano informado não existe",
            'candidate_id.required' => "O cadidato é requerido.",
            'candidate_id.exists' => "O candidato informado não existe",
            'automatic_renew.boolean' => "O valor para renovação automatica está fora do padrão (0 ou 1).",
            'hiring_start.required' => 'A data inicial da contratação deve ser informada.',
            'hiring_start.date' => 'A data ínicial está fora do padrão.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($request) {
            $candidate = \App\Candidate::find($request->get('candidate_id'));
            foreach ($candidate->plan as $plan) {
                if ($plan->id == $request->get('plan_id'))
                    $validator->errors()->add('plan_id', 'O candidato informado já possue o plano.');
            }
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $hirePlan = CandidatePlan::create($request->all());
        return response()->json(['error' => 0, "msg" => "O plano foi contratado com sucesso", "data" => $hirePlan], 200);
    }

    /**
     * Atualiza o contrato do plano
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'plan_id' => 'required|exists:plans,id',
            'candidate_id' => 'required|exists:candidates,id',
            'automatic_renew' => 'boolean',
            'hiring_start' => 'required|date',
            'hiring_end' => 'date',
        ];
        $messages = [
            'plan_id.required' => "O plano é requerido.",
            'plan_id.exists' => "O plano informado não existe",
            'candidate_id.required' => "O cadidato é requerido.",
            'candidate_id.exists' => "O candidato informado não existe",
            'automatic_renew.boolean' => "O valor para renovação automatica está fora do padrão (0 ou 1).",
            'hiring_start.required' => 'A data inicial da contratação deve ser informada.',
            'hiring_start.date' => 'A data ínicial está fora do padrão.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($id) {
            if (empty(CandidatePlan::find($id))) {
                $validator->errors()->add('id', 'O contrato de aquisição do plano não existe.');
            }
        });
        $validator->after(function ($validator) use ($request) {
            $candidate = \App\Candidate::find($request->get('candidate_id'));
            foreach ($candidate->plan as $plan) {
                if ($plan->id == $request->get('plan_id'))
                    $validator->errors()->add('plan_id', 'O candidato informado já possue o plano at.');
            }
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $candidatePlan = CandidatePlan::find($id);
        $candidatePlan->plan_id = $request->get('plan_id');
        $candidatePlan->candidate_id = $request->get('candidate_id');
        $candidatePlan->automatic_renew = $request->get('automatic_renew');
        $candidatePlan->hiring_start = $request->get('hiring_start');
        $candidatePlan->hiring_end = $request->get('hiring_end');
        $candidatePlan->save();
        return response()->json(['error' => 0, "msg" => "O contrato do plano foi atualizado com sucesso", "data" => $candidatePlan], 200);

    }
    /**
     * Exibi um registro
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json(['error' => 0, "msg" => "", "data" => CandidatePlan::find($id)], 201);
    }
    /**
     * Remove um registro
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $candidatePlan = CandidatePlan::find($id);
        if(empty($candidatePlan))
            return response()->json(['error' => 1, "msg" => "Registro não encontrado.", "data" => $candidatePlan], 201);
        $candidatePlan->delete();
        return response()->json(['error' => 0, "msg" => "Registro removido com sucesso.", "data" => $candidatePlan], 201);
    }

//<<<<<<< HEAD
//=======


//>>>>>>> origin/dev/#28-email_marketing
}
