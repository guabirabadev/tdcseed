<?php

namespace App\Http\Controllers;

use App\Repositories\TwitterRepository;
use Illuminate\Http\Request;

class TweeterController extends Controller
{
    /**
     * @var TwitterRepository
     */
    private $repository;

    public function __construct(TwitterRepository $repository)
    {

        $this->repository = $repository;
    }
    public function index()
    {
        //Auth::user()->id; Quando estiver usando usuarios registrados no sistema
        $tweets = $this->repository->paginate(10);
        return $tweets;
    }

    public function store(Request $request)
    {
        $tweet = $request->all();
        $this->repository->create($tweet);
        return response()->json($tweet, 201);
    }

    public function show($id)
    {
        $tweetSpecific = $this->repository->find($id);
        return $tweetSpecific;
    }

    public function update(Request $request, $id)
    {
        $toUpdate = $request->all();
        $this->repository->update($toUpdate, $id);
        return response()->json($toUpdate);
    }

    public function destroy($id)
    {
        $destroy = $this->repository->delete($id);
        return response()->json($destroy, 204);
    }

}
