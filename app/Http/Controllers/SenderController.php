<?php

namespace App\Http\Controllers;

use App\Sender;
use Illuminate\Http\Request;

class SenderController extends Controller
{
    /**
     * Lista todos os senders
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['error' => 0, 'msg' => "", 'data' => Sender::all()], 200);
    }

    /**
     * @param Request $request
     * @param Sender $sender
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request,Sender $sender)
    {
        $rules = [
            'nick_name' => 'required',
            'from' => 'email|required',
            'replay_to' => 'email|required',
            'replay_to_name'=>'required',
            'address' => 'required',
            'city_id' => 'exists:cities,id|required',
            'state_id' => 'exists:states,id|required',
            'candidate_id' => 'exists:candidates,id|required',
            'country' => 'required|string',
            'verified' => 'boolean',
            'locked' => 'boolean'
        ];
        $messages = [
            'nick_name.required' => 'O Apelido é requerido.',
            'from.required' => 'Remetente é requerido.',
            'from.email' => 'Remetente deve ser um e-mail válido.',
            'from_name'=>'O nome para o e-mail do remetente é requerido.',
            'replay_to.email' => 'Responder para, deve ser um e-mail váçido.',
            'replay_to.required' => 'O e-mail para resposta é requerido.',
            'replay_to_name'=>'O nome para o e-mail de resposta é requerido.',
            'address.required' => 'O endereço é requerido.',
            'city_id.exists' => 'A cidade informada não existe.',
            'city_id.required' => 'O cidade é requerida.',
            'state_id.exists' => 'O estado informado não existe.',
            'state_id.required' => 'O estado é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
            'country.required' => 'O país é requerido.',
            'country.string' => 'O país deve ser um texto.',
            'verified.boolean' => 'Verificado deve ser boolean.',
            'locked.boolean' => 'Verificado deve ser boolean.'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $sender->fill($request->all());
        $sender->save();
        return response()->json(['error' => 0, "msg" => "Sender criado", "data" => $validator->errors()], 201);

    }
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id){
        $sender = Sender::find($id);
        if(!$sender)
            return response()->json(['error' => 1, "msg" => "Sender não encontrado.", "data" => ""], 400);
        $rules = [
            'nick_name' => 'required',
            'from' => 'email|required',
            'replay_to' => 'email|required',
            'replay_to_name'=>'required',
            'address' => 'required',
            'city_id' => 'exists:cities,id|required',
            'state_id' => 'exists:states,id|required',
            'candidate_id' => 'exists:candidates,id|required',
            'country' => 'required|string',
            'verified' => 'boolean',
            'locked' => 'boolean'
        ];
        $messages = [
            'nick_name.required' => 'O Apelido é requerido.',
            'from.required' => 'Remetente é requerido.',
            'from.email' => 'Remetente deve ser um e-mail válido.',
            'from_name'=>'O nome para o e-mail do remetente é requerido.',
            'replay_to.email' => 'Responder para, deve ser um e-mail váçido.',
            'replay_to.required' => 'O e-mail para resposta é requerido.',
            'replay_to_name'=>'O nome para o e-mail de resposta é requerido.',
            'address.required' => 'O endereço é requerido.',
            'city_id.exists' => 'A cidade informada não existe.',
            'city_id.required' => 'O cidade é requerida.',
            'state_id.exists' => 'O estado informado não existe.',
            'state_id.required' => 'O estado é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
            'country.required' => 'O país é requerido.',
            'country.string' => 'O país deve ser um texto.',
            'verified.boolean' => 'Verificado deve ser boolean.',
            'locked.boolean' => 'Verificado deve ser boolean.'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $sender->fill($request->all());
        $sender->save();
        return response()->json(['error' => 0, "msg" => "Sender criado", "data" => $validator->errors()], 201);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $sender = Sender::find($id);
        if(!$sender)
            return response()->json(['error' => 1, "msg" => "Sender não encontrado.", "data" => ""], 400);
        $sender->delete();
        return response()->json(['error' => 0, "msg" => "Sender removido.", "data" => ""], 200);
    }
}
