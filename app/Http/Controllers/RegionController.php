<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Region;

class RegionController extends Controller
{
    /**
     * Retorna todas as regiões cadastradas.
     */
    public function index()
    {
        return Region::all();
    }


    /**
     * Metodo para criar uma nova região.
     */
    public function store(Request $request)
    {
        $region = Region::create($request->all());
        return response()->json($region, 201);
    }

    /**
     * Metodo para mostrar uma região de um ID especifico.
     */
    public function show(Region $region)
    {
        return $region;
    }


    /**
     * Metodo para atualizar região selecionando um ID especifico.
     */
    public function update(Request $request, Region $region)
    {
        $region->fill($request->all());
        $region->save();
        return response()->json($region);
    }

    /**
     * Metodo para remover região selecionando um ID especifico.
     */
    public function destroy(Region $region)
    {
        $region->delete();
        return response()->json($region, 204);
    }
}
