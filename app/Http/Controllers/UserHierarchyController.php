<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserHierarchy as Hierachy;

class UserHierarchyController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Hierachy::all();
    }

    /**
     * Check if ID user is child
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function check( $id )
    {
        if (! $user = \JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $arr = search_child(\Auth::user()->hierarchy, $id);
        return response()->json( $arr, 201 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $data = Hierachy::create( $request->all() );
        return response()->json( $data, 201 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $data = Hierachy::find( $id );
        return response()
            ->json( $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $hierarchy = Hierachy::find( $id );
        $hierarchy->fill( $request->all() );
        $hierarchy->save();
        return response()
            ->json($hierarchy);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $hierarchy = Hierachy::find( $id );
        $hierarchy->delete();
        return response()
            ->json( [], 204 );
    }
}
