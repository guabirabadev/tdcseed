<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class PlanController extends Controller
{
    /**
     * Método para mostrar todos os planos
     */
    public function index()
    {
        return response()->json(['error' => 0, "msg" => "", "data" => Plan::all()]);
    }

    /**
     * Método para criar novo plano
     */
    public function store(Request $request)
    {
        $rules = [
            'max_users' => 'required|integer',
            'max_electors' => 'required|integer',
            'sms_quantity' => 'required|integer',
            'emails_quantity' => 'required|integer',
            'price'=>'required',
            'renewal_frequency' => 'required|integer',
            'name' => 'required|unique:plans,name',
        ];
        $messages = [
            'max_user.required'=>'A quantidade máxima de usuários e requerida.',
            'max_user.integer'=>'O valor para a quantidade máxima de usuários deve ser um número inteiro e positivo.',
            'max_electors.required'=>'A quantidade máxima de eleitores e requerida.',
            'max_electors.integer'=>'O valor para a quantidade máxima de eleitores deve ser um número inteiro e positivo.',
            'sms_quantity.required'=>'A quantidade máxima de sms e requerida.',
            'sms_quantity.integer'=>'O valor para a quantidade máxima de sms deve ser um número inteiro e positivo.',
            'emails_quantity.required'=>'A quantidade máxima de e-mails e requerida.',
            'emails_quantity.integer'=>'O valor para a quantidade máxima de e-mails deve ser um número inteiro e positivo.',
            'price.required'=>'O preço do plano é requerido.',
            'renewal_frequency.required'=>'A quantidade em dias da frequência de renovação e requerida.',
            'renewal_frequency.integer'=>'O valor para a frequência de renovção deve ser um número inteiro e positivo.',
            'name.required'=>'O nome do plano é requerido.',
            'name.unique'=>'O nome do plano já está cadastrado, tente outro por favor.'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()],400);
        }
        $plan = Plan::create($request->all());
        return response()->json(['error' => 0, "msg" => "", "data" => $plan], 201);
    }

    /**
     * Método para exibir plano de id específico
     */
    public function show(Plan $plan)
    {
        return response()->json(['error' => 0, "msg" => "", "data" => $plan], 201);
    }

    /**
     * Método para atualizar plano existente
     */
    public function update(Request $request, Plan $plan)
    {
        $rules = [
            'max_users' => 'required|integer',
            'max_electors' => 'required|integer',
            'sms_quantity' => 'required|integer',
            'emails_quantity' => 'required|integer',
            'price'=>'required',
            'renewal_frequency' => 'required|integer',
            'name' => 'required',
        ];
        $messages = [
            'max_user.required'=>'A quantidade máxima de usuários e requerida.',
            'max_user.integer'=>'O valor para a quantidade máxima de usuários deve ser um número inteiro e positivo.',
            'max_electors.required'=>'A quantidade máxima de eleitores e requerida.',
            'max_electors.integer'=>'O valor para a quantidade máxima de eleitores deve ser um número inteiro e positivo.',
            'sms_quantity.required'=>'A quantidade máxima de sms e requerida.',
            'sms_quantity.integer'=>'O valor para a quantidade máxima de sms deve ser um número inteiro e positivo.',
            'emails_quantity.required'=>'A quantidade máxima de e-mails e requerida.',
            'emails_quantity.integer'=>'O valor para a quantidade máxima de e-mails deve ser um número inteiro e positivo.',
            'price.required'=>'O preço do plano é requerido.',
            'renewal_frequency.required'=>'A quantidade em dias da frequência de renovação e requerida.',
            'renewal_frequency.integer'=>'O valor para a frequência de renovção deve ser um número inteiro e positivo.',
            'name.required'=>'O nome do plano é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()],400);
        }
        $plan->fill($request->all());
        $plan->save();
        return response()->json(['error' => 0, "msg" => "", "data" => $plan], 201);
    }

    /**
     * Método para excluir plano de id específico
     */
    public function destroy(Plan $plan)
    {
        $plan->delete();
        return response()->json($plan, 204);
    }
}
