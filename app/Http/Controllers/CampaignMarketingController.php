<?php

namespace App\Http\Controllers;

use App\CampaignMarketing;
use App\Candidate;
use App\Jobs\RenderTemplateMarketing;
use App\Jobs\SendEmailMarketing;
use App\ListMarketing;
use App\Sender;
use App\TemplateEmailMarketing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CampaignMarketingController extends Controller
{
    public function store(Request $request, CampaignMarketing $campaignMarketing)
    {
        $rules = [
            'name' => 'required',
            'sender_id' => 'required|exists:senders,id',
            'template_email_marketing_id' => 'required|exists:template_email_marketings,id',
            'list_marketing_id' => 'required||exists:list_marketing,id',
            'candidate_id' => 'exists:candidates,id|required',
            'scheduling' => 'nullable|date|after:today'
        ];
        $messages = [
            'name.required' => 'O nome é requerido para a campanha.',
            'sender_id.exists' => 'O sender informado não existe.',
            'sender_id.required' => 'O sender é requerido.',
            'template_email_marketing_id.required' => 'O template é requerido.',
            'template_email_marketing_id.exists' => 'O template informado não existe',
            'list_marketing_id.required' => 'A lista de contatos é requerida.',
            'list_marketing_id.exists' => 'A lista informada não existe.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
            'scheduling.date' => 'O agendamento está fora do padrão americano.',
            'scheduling.after' => 'A data e hora do agendamento deve ser maior que data atual.'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {
            $validator->after(function ($validator) use ($request) {
                $candidate = Candidate::find($request->get('candidate_id'));

                //Check Sender
                if (!$candidate->hasSender(Sender::find($request->get('sender_id')))) {
                    $validator->errors()->add('sender_id', 'O sender informado não pertence ao candidato.');
                }

                //Check template
                if (!$candidate->hasTemplate(TemplateEmailMarketing::find($request->get('template_email_marketing_id')))) {
                    $validator->errors()->add('template_email_marketing_id', 'O template informado não pertence ao candidato.');
                }

                //Check list
                if (!$candidate->hasListMarketing(ListMarketing::find($request->get('list_marketing_id')))) {
                    $validator->errors()->add('template_email_marketing_id', 'O lista informada não pertence ao candidato.');
                }

                //check has hire plan
                //
                if (!$candidate->plans()) {
                    $validator->errors()->add('plans', 'O candidato não tem nenhum plano cadastrado');
                }
                //check shudeling
                if ($request->has('scheduling')) {
                    $scheduling = new Carbon($request->get('scheduling'));
                    if ($scheduling < Carbon::now())
                        $validator->errors()->add('scheduling',
                            'A data e hora do agendamento deve ser maior que data atual.');
                }

            });
        }
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }

        $campaignMarketing->fill($request->all());
        $campaignMarketing->save();
        /**
         * Verificar se existe agendamento
         *  * Se for do dia colocar na fila
         *  * Se não for não fazer nenhuma ação, exisitira um job para agendar a tarefa
         */

        $this->renderCampaign($campaignMarketing);
        if ($request->has('scheduling')) {
            $scheduling = new Carbon($request->get('scheduling'));
            $diff = $scheduling->diff(Carbon::now());
            if ($diff->d == 0 && $diff->m == 0 && $diff->y == 0) {
                $job = (new SendEmailMarketing($campaignMarketing))
                    ->delay($scheduling);
                $campaignMarketing->job_id = dispatch($job);
                $campaignMarketing->save();
            }
        }
        return response()->json(['error' => 0, "msg" => "Campanha agendada.", "data" => ""], 201);
        $this->send($campaignMarketing->id);

    }

    /**
     * Render template of campaign
     * @param CampaignMarketing $campaignMarketing
     * @return void
     */
    private function renderCampaign(CampaignMarketing $campaignMarketing)
    {
        /**
         *
         * variables on template
         *
         * [[email]] * the change on current moment to send
         * [[first_name]] * the change on current moment to send
         * [[last_name]] * the change on current moment to send
         * [[Sender_Name]]
         * [[Sender_Address]]
         * [[Sender_City]]
         * [[Sender_State]]
         * [[Sender_Zip]]
         * [[Unsubscribe]]
         */

        //changes variables
        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Sender_Name]]',
            $campaignMarketing->sender->from_name,
            $campaignMarketing->templateEmailMarketing->content_value
        );
        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Sender_Address]]',
            $campaignMarketing->sender->address,
            $campaignMarketing->templateEmailMarketing->content_value
        );

        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Sender_City]]',
            $campaignMarketing->sender->city->name,
            $campaignMarketing->templateEmailMarketing->content_value
        );

        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Sender_State]]',
            $campaignMarketing->sender->states->name,
            $campaignMarketing->templateEmailMarketing->content_value
        );

        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Sender_Zip]]',
            $campaignMarketing->sender->zip,
            $campaignMarketing->templateEmailMarketing->content_value
        );

        $campaignMarketing->templateEmailMarketing->content_value = str_replace('[[Unsubscribe]]',
            "LINK_Unsubscribe",
            $campaignMarketing->templateEmailMarketing->content_value
        );

        Storage::disk('public')->put(
            env('RENDER_EMAIL_MARKETING') . '/' . md5($campaignMarketing->id) . ".html",
            htmlspecialchars_decode($campaignMarketing->templateEmailMarketing->content_value)
        );


    }

    public function send($id)
    {
        $campaign = CampaignMarketing::find($id);
        $senders = $campaign->sender;
        $template = $campaign->templateEmailMarketing;
        $contacts = $campaign->listContacts->contacts;
        $personalizations['personalizations'] = [];
        $personalization = ['to'=>[],'subject'=>""];
        $to['to'] = [];
        $i = 0;
        foreach ($contacts as $contact):
            array_push($to['to'], ['email' => $contact->email,'name' => $contact->first_name . ' ' . $contact->last_name]);
            if($i==10)
                break;
            $i++;
        endforeach;
        $personalization['subject'] = $campaign->name;
        $personalization['to'] = $to['to'];
        array_push($personalizations['personalizations'],$personalization);

        $personalizations['from'] = ['email' => $senders->from, 'name' => $senders->from_name];
        $personalizations['content'] = [[
            'type' => 'text/plain',
            'value' => Storage::disk('public')->get(env('RENDER_EMAIL_MARKETING') . '/' . md5($template->id) . ".html")
        ]];
        $personalizations['tracking_settings'] = [
            'click_tracking' => [
                'enable'=>true,
                'enable_text'=>true

            ],
            'open_tracking' => [
                'enable'=>true
            ],
            'subscription_tracking'=>[
                'enable'=>true
            ],
            'ganalytics'=>[
                'enable'=>true
            ]
        ];

        //echo print_r($AUX);
        //echo json_encode($personalizations);
        //exit();
        $sg = new \SendGrid(env('SENDGRID_API_KEY'));
        $response = $sg->client->mail()->send()->post($personalizations);
        echo $response->statusCode();
        print_r($response->headers());
        echo $response->body();


    }
}
