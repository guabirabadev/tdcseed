<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Metodo para mostrar todos os registros.
     */
    public function index()
    {
        return Address::all();
    }

    /**
     * Metodo para criar um novo registro.
     */
    public function store(Request $request)
    {
        $address = Address::create($request->all());
        return response()->json($address, 201);
    }

    /**
     * Metodo para mostrar um registro selecionando um ID especifico.
     */
    public function show(Address $address)
    {
        return $address;
    }


    /**
     * Metodo para atualizar registro selecionando um ID especifico.
     */
    public function update(Request $request, Address $address)
    {
        $address->fill($request->all());
        $address->save();
        return response()->json($address);
    }

    /**
     * Metodo para remover registro selecionando um ID especifico.
     */
    public function destroy(Address $address)
    {
        $address->delete();
        return response()->json([], 204);
    }
}
