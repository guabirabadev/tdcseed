<?php

namespace App\Http\Controllers;

use App\Http\Requests\CotactMkCustomRequest;
use App\Repositories\ContactRepository;
use Illuminate\Http\Request;

class ContactMkController extends Controller
{

    private $repository;

    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }
    public function missingMethod($params = array())
    {
        $error = "Error 404";
        return response()->json($error, 404);
    }
    public function index (  )
    {
        $assets = $this->repository->paginate(10);
        return $assets;
    }
    public function store ( CotactMkCustomRequest $request )
    {
        $data = $request->all();
        $file = $request->file('profileAvatar');
        if(is_object($file) and $file->isValid()){
            $filename = time(). '-' . $file->getClientOriginalName();
            $file->move(public_path().'/img/hub-mkt/contact/avatars/', $filename);
            $path = $filename;
            $data['profileAvatar']=$path;
            $this->repository->create($data);
        }else{
            $err_msg =  "Avatar is required";
            return $err_msg;
        }
        return response()->json($data, 201);
    }
    public function show($id)
    {
        $item = $this->repository->find($id);
        return $item;
    }
    public function update(CotactMkCustomRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return $data;
    }
    public function destroy( $id)
    {
        $this->repository->delete($id);
        $ok = "Deleted with sucess";
        return response()->json($ok, 200);
    }
}
