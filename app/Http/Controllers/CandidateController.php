<?php

namespace App\Http\Controllers;

use App\Http\Requests\CandidateRequest;
use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    /**
     * Metodo para mostrar todos os registros.
     */
    public function index()
    {
        return Candidate::all();
    }

    /**
     * Metodo para criar um novo registro.
     */
    public function store(CandidateRequest $request)
    {
        $canditate = Candidate::create($request->all());
        return response()->json($canditate, 201);
    }

    /**
     * Metodo para mostrar um registro selecionando um ID especifico.
     */
    public function show(Candidate $candidate)
    {
        return $candidate;
    }

    /**
     * Metodo para atualizar registro selecionando um ID especifico.
     */
    public function update(CandidateRequest $request, Candidate $candidate)
    {
        $candidate->fill($request->all());
        $candidate->save();
        return response()->json($candidate);
    }

    /**
     * Metodo para remover registro selecionando um ID especifico.
     */
    public function destroy(Candidate $candidate)
    {
        $candidate->delete();
        return response()->json([], 204);
    }
}
