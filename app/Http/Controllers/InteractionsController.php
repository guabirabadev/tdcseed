<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\InteractionsCreateRequest;
use App\Http\Requests\InteractionsUpdateRequest;
use App\Repositories\InteractionsRepository;
use App\Validators\InteractionsValidator;


class InteractionsController extends Controller
{

    /**
     * @var InteractionsRepository
     */
    protected $repository;

    /**
     * @var InteractionsValidator
     */
    protected $validator;

    public function __construct(InteractionsRepository $repository, InteractionsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $interactions = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $interactions,
            ]);
        }

        return view('interactions.index', compact('interactions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  InteractionsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(InteractionsCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $interaction = $this->repository->create($request->all());

            $response = [
                'message' => 'Interactions created.',
                'data'    => $interaction->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $interaction = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $interaction,
            ]);
        }

        return view('interactions.show', compact('interaction'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $interaction = $this->repository->find($id);

        return view('interactions.edit', compact('interaction'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  InteractionsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(InteractionsUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $interaction = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Interactions updated.',
                'data'    => $interaction->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Interactions deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Interactions deleted.');
    }
}
