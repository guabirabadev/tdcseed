<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\State;

class StateController extends Controller
{
    /**
     * Retorna todos os estados cadastrados ou os estados de uma região especifica.
     * Para retornar os estados de região especifica, basta passar o parametro region com o ID da região
     * Exemplo: http://127.0.0.1:8000/api/states?region=2
     */
    public function index(Request $request)
    {
        if($request->input('region')){
            return State::where('region_id', $request->input('region'))->get();
        }

        return State::all();
    }

    /**
     * Metodo para criar um novo estado.
     */
    public function store(Request $request)
    {
        $state = State::create($request->all());
        return response()->json($state, 201);
    }

    /**
     * Metodo para mostrar um estado de um ID especifico.
     */
    public function show(State $state)
    {
        return $state;
    }


    /**
     * Metodo para atualizar estado selecionando um ID especifico.
     */
    public function update(Request $request, State $state)
    {
        $state->fill($request->all());
        $state->save();
        return response()->json($state);
    }

    /**
     * Metodo para remover estado selecionando um ID especifico.
     */
    public function destroy(State $state)
    {
        $state->delete();
        return response()->json($state, 204);
    }
}
