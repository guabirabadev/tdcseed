<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use App\Models\Voter;
use Illuminate\Support\Facades\Auth;
use \JWTAuth;

class VoterController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Voter::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $data = [];

        //Get the authenticated user by token
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $voter = new Voter();
        $voter->fill( $request->all() );

        //Set the user_id to the authenticated user id
        $voter->user_id = $user->id;

        if( $voter->save() ) {
            $data['voter'] = $voter;
        }

        if ( $request->file('file') ) {
            $file = $request->file('file');
            $treatment = new \App\Helpers\Files($request->file('file'));

            if ($file->move($treatment->file_dir, $treatment->file_disk)) {
                $dataFile = $voter->image()->create($treatment->getFileData());

                if( $dataFile ) {
                    $data['file'] = $dataFile;
                }
            }
        }

        if ( $request->input('education') ) {
            $education = $voter->education()->create([
                'education' => $request->input('education')
            ]);

            $data['education'] = $education;
        }

        return response()->json( $data, 201 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $data = Voter::find( $id );
        return response()
            ->json( $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Voter $voter
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, Voter $voter )
    {
        $voter->fill( $request->all() );

        if( $request->input('education') ) {
            $voter->education()->fill( $request->input('education') );
        }

        if( $request->file('file') ) {
            $voter->image()->first()->delete();

            // set new file
            $file = $request->file('file');
            $treatment = new \App\Helpers\Files($request->file('file'));

            if ($file->move($treatment->file_dir, $treatment->file_disk)) {
                $dataFile = $voter->image()->create($treatment->getFileData());

                if( $dataFile ) {
                    $data['file'] = $dataFile;
                }
            }
        }

        $voter->save();
        return response()
            ->json($voter);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $voter = Voter::find( $id );
        if( $voter->education()->first() ) {
            $voter->education()->delete();
        }
        if( $voter->image()->first() ) {
            $voter->image()->delete();
        }
        $voter->delete();
        return response()
            ->json( [], 204 );
    }
}
