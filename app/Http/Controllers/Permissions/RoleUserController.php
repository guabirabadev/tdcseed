<?php
/**
 * Created by PhpStorm.
 * User: Wellington
 * Date: 21/02/2017
 * Time: 19:29
 */

namespace App\Http\Controllers\Permissions;


use App\Http\Controllers\Controller;
use App\RoleUser;
use App\User;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class RoleUserController extends Controller
{
    /**
     * Lista todos os usuários com suas permissões
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all(){
        $RolesUser = User::all();
        foreach($RolesUser as $Key => $Value) {
            $RolesUser[$Key]->roles = $Value->roles()->get();
        }
        return $RolesUser;
    }
    /**
     * Inserido uma nova permissão
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insert(Request $request){
        $rules = [
            'user_id' => 'exists:users,id|required',
            'role_id' => 'exists:roles,id|required'
        ];
        $messages = [
            'user_id.exists' => 'O id do usuário não existe',
            'user_id.required' => 'O id do usuário e requerido',
            'role_id.exists' => 'O role não existe',
            'role_id.required' => 'O id da role é requerido',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $CheckRoleUser = DB::table('role_user')->where('user_id', '=', $request->get('user_id'))
            ->where('role_id', '=', $request->get('role_id'))->count();
        $validator->after(function ($validator) use ($CheckRoleUser) {
            if ($CheckRoleUser>0)
                $validator->errors()->add('permission_id', 'A permissão para o usuário já existe');

        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $RoleUser = new RoleUser();
        $RoleUser->user_id = $request->get('user_id');
        $RoleUser->role_id = $request->get('role_id');
        $RoleUser->save();
        return response()->json(['error' => 0, "msg" => "Permissão inserida com sucesso", "data" => $RoleUser]);
    }

    /**
     * Atualização das permissões dos usuários
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        $rules = [
            'id'=>'exists:role_user|required',
            'user_id' => 'exists:users,id|required',
            'role_id' => 'exists:roles,id|required'
        ];
        $messages = [
            'id.exists' => 'O id do permissão não foi encontrado',
            'id.required' => 'O id do permissão é requerido',
            'user_id.exists' => 'O id do usuário não existe',
            'user_id.required' => 'O id do usuário é requerido',
            'role_id.exists' => 'O role não existe',
            'role_id.required' => 'O id da role é requerido',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $CheckRoleUser = DB::table('role_user')->where('user_id', '=', $request->get('user_id'))
            ->where('role_id', '=', $request->get('role_id'))->count();

        $validator->after(function ($validator) use ($CheckRoleUser) {
            if ($CheckRoleUser>0)
                $validator->errors()->add('permission_id', 'A permissão para o usuário já existe');

        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $RoleUser = RoleUser::find($request->get('id'));
        $RoleUser->user_id = $request->get('user_id');
        $RoleUser->role_id = $request->get('role_id');
        $RoleUser->save();
        return response()->json(['error' => 0, "msg" => "Permissão atualizada com sucesso", "data" => $RoleUser]);

    }
    /**
     * Apaga a permissão do usuário
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        if(!is_numeric($id)){
            return response()->json(['error' => 1, "msg" => "Id fora do padrão", "data" => ""]);
        }
        $RoleUser = DB::table('role_user')->where('id', '=',$id);
        if($RoleUser->count() ==0){
            return response()->json([
                'error' => 1,
                "msg" => "A permissão foi encontrada",
                "data" => ""
            ]);
        }
        $RoleUser->delete();
        return response()->json([
            'error' => 0,
            "msg" => "A permissão foi apagada",
            "data" => ""
        ]);
    }

}