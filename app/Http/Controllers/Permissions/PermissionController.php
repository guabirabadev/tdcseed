<?php
namespace App\Http\Controllers\Permissions;

use App\Permission;
use Illuminate\Routing\Controller;
use \Symfony\Component\HttpFoundation\Request;


class PermissionController extends Controller
{
    /**
     * Retorna todas as permissões
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json(['error' => 0, "msg" => "", "data" => Permission::all()]);
    }

    /**
     * Insert de permissões
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insert(Request $request)
    {
        $rules = [
            'name' => 'required|max:200|unique:permissions',
            'label' => 'required|max:1000',
        ];
        $messages = [
            'name.required' => "O nome da permissão é requerida.",
            'name.max' => "O tamanho máximo para o campo \"Nome\" é de 200 caracteres. ",
            'name.unique' => "A permissão já existe.",
            'label.max' => "O tamanho máximo para o campo \"Label\" é de 1000 caracteres. ",
            'label.required' => "O label da permissão é requerido."
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $Permission = new Permission();
        $Permission->name = $request->get('name');
        $Permission->label = $request->get('label');
        $Permission->save();
        return response()->json(['error' => 0, "msg" => "Permissão inserida com sucesso", "data" => $Permission]);
    }


    /**
     * Atualização do Permissão
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'exists:permissions|required',
            'name' => 'required|max:200',
            'label' => 'required|max:1000',
        ];
        $messages = [
            'id.exists' => 'O permissão não existe',
            'id.required' => 'O id da permissão é requerida',
            'name.required' => "O nome da permissão é requerida.",
            'name.max' => "O tamanho máximo para o campo \"Nome\" é de 200 caracteres. ",
            'name.unique' => "A permissão já existe.",
            'label.max' => "O tamanho máximo para o campo \"Label\" é de 1000 caracteres. ",
            'label.required' => "O label da permissão é requerido."
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $Permission = Permission::find($request->get('id'));
        $Permission->name = $request->get('name');
        $Permission->label = $request->get('label');
        $Permission->save();
        return response()->json(['error' => 0, "msg" => "", "data" => $Permission]);
    }

    /**
     * Para apagar permissão
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error' => 1, "msg" => "Id fora do padrão", "data" => ""]);
        }

        $Permission = Permission::find($id);
        if(empty($Permission)){
            return response()->json(['error' => 1, "msg" => "Permissão não encontrada", "data" => ""]);
        }
        $Permission->delete();
        return response()->json(['error' => 1, "msg" => "Permissão deletada com sucesso", "data" => ""]);
    }


}