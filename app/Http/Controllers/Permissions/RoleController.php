<?php
/**
 * Created by PhpStorm.
 * User: Wellington
 * Date: 21/02/2017
 * Time: 17:34
 */

namespace App\Http\Controllers\Permissions;


use App\Role;
use Illuminate\Routing\Controller;
use \Symfony\Component\HttpFoundation\Request;

class RoleController extends Controller
{
    /**
     * Lista todas as roles
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json(['error' => 0, "msg" => "", "data" => Role::all()]);
    }
    /**
     * Insert de role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insert(Request $request){

        $rules = [
            'name' => 'required|max:200|unique:roles',
            'label' => 'required|max:1000',
        ];
        $messages = [
            'name.required' => "O nome da role é requerido.",
            'name.max' => "O tamanho máximo para o campo \"Nome\" é de 200 caracteres. ",
            'name.unique' => "A role já existe.",
            'label.max' => "O tamanho máximo para o campo \"Label\" é de 1000 caracteres. ",
            'label.required' => "O label da role é requerido."
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $Permission = new Role();
        $Permission->name = $request->get('name');
        $Permission->label = $request->get('label');
        $Permission->save();
        return response()->json(['error' => 0, "msg" => "Role inserida com sucesso", "data" => $Permission]);
    }

    /**
     * Atualiza um role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'exists:roles|required',
            'name' => 'required|max:200',
            'label' => 'required|max:1000',
        ];
        $messages = [
            'id.exists' => 'O role não exise',
            'id.required' => 'O id da role é requerido',
            'name.required' => "O nome da role é requerido.",
            'name.max' => "O tamanho máximo para o campo \"Nome\" é de 200 caracteres. ",
            'label.max' => "O tamanho máximo para o campo \"Label\" é de 1000 caracteres. ",
            'label.required' => "O label da role é requerida."
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $Role = Role::find($request->get('id'));
        $Role->name = $request->get('name');
        $Role->label = $request->get('label');
        $Role->save();
        return response()->json(['error' => 0, "msg" => "Role atualizada com sucesso", "data" => $Role]);
    }
    /**
     * Apaga role
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error' => 1, "msg" => "Id fora do padrão", "data" => ""]);
        }
        $Role = Role::find($id);
        if(empty($Role)){
            return response()->json(['error' => 1, "msg" => "Role não encontrada", "data" => ""]);
        }
        $Role->delete();
        return response()->json(['error' => 0, "msg" => "Role apagada com sucesso", "data" => ""]);
    }


}