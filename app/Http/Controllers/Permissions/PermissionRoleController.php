<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\DB;
use \Symfony\Component\HttpFoundation\Request;

class PermissionRoleController extends Controller
{
    /**
     * Lista todas as permissões associadas
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(){
        $CheckRole = DB::table('permission_role')
            ->select([
                'permission_role.id','permission_role.permission_id',
                'permission_role.role_id','permissions.name as permissions_name',
                'permissions.label as permissions_label','roles.name as roles_name',
                'roles.label as roles_label'
            ])
            ->join('permissions',function($inner){
                $inner->on('permissions.id','=','permission_role.permission_id');
            })
            ->join('roles',function($inner){
                $inner->on('roles.id','=','permission_role.role_id');
            })->get();
        return response()->json(['error' => 0, "msg" => "", "data" => $CheckRole]);
    }
    /**
     * Associação de permissão a role
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insert(Request $request)
    {
        $rules = [
            'permission_id' => 'exists:permissions,id|required',
            'role_id' => 'exists:roles,id|required'
        ];
        $messages = [
            'permission_id.exists' => 'O permissão não existe',
            'permission_id.required' => 'O id da permissão é requerida',
            'role_id.exists' => 'O role não existe',
            'role_id.required' => 'O id da role é requerido',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $CheckRole = DB::table('permission_role')->where('permission_id', '=', $request->get('permission_id'))
            ->where('role_id', '=', $request->get('role_id'))->count();
        $validator->after(function ($validator) use ($CheckRole) {
            if ($CheckRole>0)
                $validator->errors()->add('permission_id', 'A associação entre permissão e a role já existe');
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $Permission = Permission::find($request->get('permission_id'));
        $Role = Role::find($request->get('role_id'));
        $Permission->roles()->attach($Role);
        return response()->json(['error' => 0, "msg" => "Permissão atualizada com sucesso", "data" => ""]);
    }
    /**
     * Atualiza a associação entre a permissão e as roles
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){
        $rules = [
            'id'=>'exists:permission_role|required',
            'permission_id' => 'exists:permissions,id|required',
            'role_id' => 'exists:roles,id|required'
        ];
        $messages = [
            'id.exists' => 'O id da associção entre a permissão e role não existe',
            'id.required' => 'O id da associção entre a permissão e role é requerido',
            'permission_id.exists' => 'O permissão não existe',
            'permission_id.required' => 'O id da permissão é requerido',
            'role_id.exists' => 'O role não existe',
            'role_id.required' => 'O id da role é requerido',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $CheckRole = DB::table('permission_role')->where('permission_id', '=', $request->get('permission_id'))
            ->where('role_id', '=', $request->get('role_id'))->count();
        $validator->after(function ($validator) use ($CheckRole) {
            if ($CheckRole>0)
                $validator->errors()->add('permission_id', 'A associção entre a permissão e role  já existe');

        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        //Atualizando a associação
        DB::table('permission_role')->where('id', '=',$request->get('id'))
            ->update([
                'permission_id'=>$request->get('permission_id'),
                'role_id'=>$request->get('role_id')
            ]);

        return response()->json([
            'error' => 0,
            "msg" => "associção entre a permissão e role  atualizada com sucesso",
            "data" => ""
        ]);
    }

    /**
     * Apaga a associção  entre a permissão e role
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        if(!is_numeric($id)){
            return response()->json(['error' => 1, "msg" => "Id fora do padrão", "data" => ""]);
        }
        $PermissionRole = DB::table('permission_role')->where('id', '=',$id);
        if($PermissionRole->count() ==0){
            return response()->json([
                'error' => 1,
                "msg" => "Id da  associção entre a permissão e role não foi encontrada",
                "data" => ""
            ]);
        }
        $PermissionRole->delete();
        return response()->json([
            'error' => 0,
            "msg" => "Associção entre a permissão e role apagada com sucesso",
            "data" => ""
        ]);
    }

}