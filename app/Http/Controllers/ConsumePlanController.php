<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Consume;
use App\Plan;
use Illuminate\Http\Request;

class ConsumePlanController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'plan_id' => 'exists:plans,id',
            'candidate_id' => 'exists:candidates,id|required',
            'description_consume' => 'required',
            'sms_consume' => 'integer|required',
            'email_consume' => 'integer|required'
        ];
        $messages = [
            'candidate_id.exists' => 'O candidato informado não existe',
            'candidate.required' => 'O candidato é requerido',
            'description_consume.required' => 'Informe o descrição do consumo',
            'sms_consume.integer' => 'Informe o consumo de sms',
            'email_consume' => 'Informe o consumo de e-mail'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        //Verificando planos
        $validator->after(function ($validator) use ($request) {
            $candidate = \App\Candidate::find($request->get('candidate_id'));
            if ($candidate->plans()->count() == 0)
                $validator->errors()->add('candidate_id', 'O candidato não tem um plano válido');
        });

        $candidate = new Candidate();
        $candidate = $candidate->find($request->get('candidate_id'));
        $planConsume = null;
        //Verifica se tem algum plano
        if ($request->has('plan_id')) {
            $validator->after(function ($validator) use ($request, $candidate) {
                if (!$candidate->hasPlan(Plan::find($request->get('plan_id'))))
                    $validator->errors()->add('plan_id', 'O candidato não possui o plano informado');
            });
            $candidatePlan = $candidate->getCandidatePlan(Plan::find($request->get('plan_id')));
            $planConsume = $candidatePlan->currentConsume();
        }else{
            $planConsume = $candidate->smallerConsume($request->get('sms_consume'), $request->get('email_consume'));
        }
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        //Salva o consumo
        $consume = new Consume();
        $consume->sms_consume = $request->get('sms_consume');
        $consume->email_consume = $request->get('email_consume');
        $consume->description_consume = $request->get('description_consume');
        $consume->plan_consume_id = $planConsume->id;
        $consume->save();
        $planConsume->sms_consume += $request->get('sms_consume');
        $planConsume->email_consume += $request->get('email_consume');
        $planConsume->save();
        return response()->json(['error' => 0, "msg" => "", "data" => $consume], 201);
    }

    /**
     * Exibi um consumo pelo id
     * @param Consume $consume
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Consume $consume)
    {
        return response()->json(['error' => 0, "msg" => "", "data" => $consume], 201);
    }

    /**
     * Apaga um consumo
     * @param Consume $consume
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Consume $consume)
    {
        $consume->delete();
        return response()->json($consume, 204);
    }
}
