<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompaniesCustomRequest;
use App\Repositories\CompaniesRepository;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * @var CampaignRepository
     */
    private $repository;

    public function __construct(CompaniesRepository $repository)
    {

        $this->repository = $repository;
    }

    public function missingMethod($params = array())
    {
        $error = "Error 404";
        return response()->json($error, 404);
    }

    public function index (  )
    {
        $assets = $this->repository->paginate(10);
        return $assets;
    }

    public function store ( CompaniesCustomRequest $request )
    {
        $data = $request->all();
        $this->repository->create($data);
        return $data;
    }

    public function show($id)
    {
        $item = $this->repository->find($id);
        return $item;
    }

    public function update(CompaniesCustomRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return $data;
    }


    public function destroy( $id)
    {
        $this->repository->delete($id);
        return response()->json($id, 204);
    }
}
