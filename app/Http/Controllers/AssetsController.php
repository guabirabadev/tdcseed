<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssetCustomRequest;
use App\Models\Asset;
use App\Repositories\AssetRepository;
use Illuminate\Http\Request;

class AssetsController extends Controller
{
    /**
     * @var AssetRepository
     */
    private $repository;

    public function __construct(AssetRepository $repository)
    {
        $this->repository = $repository;
    }

    public function missingMethod($params = array())
    {
        $error = "Error 404";
        return response()->json($error, 404);
    }

    public function index (  )
    {
        $assets = $this->repository->paginate(10);
        return $assets;
    }

    public function store ( AssetCustomRequest $request )
    {
        $data = $request->all();
        $file = $request->file('path');
        if(is_object($file) and $file->isValid()){
            $filename = time(). '-' . $file->getClientOriginalName();
            $file->move(public_path().'/img/hub-mkt/assets/', $filename);
            $path = $filename;
            $data['path']=$path;
            $this->repository->create($data);
        }else{
            $err_msg =  "Image is required";
            return $err_msg;
        }
    }

    public function show($id)
    {
        $item = $this->repository->find($id);
        return $item;
    }

    public function update(AssetCustomRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return $data;
    }


    public function destroy( $id)
    {
        $this->repository->delete($id);
        return response()->json($id, 204);
    }
}
