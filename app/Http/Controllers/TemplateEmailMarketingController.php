<?php

namespace App\Http\Controllers;

use App\TemplateEmailMarketing;
use Illuminate\Http\Request;

class TemplateEmailMarketingController extends Controller
{
    /**
     * @param Request $request
     * @param TemplateEmailMarketingController $templateEmailMarketingController
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, TemplateEmailMarketing $templateEmailMarketing)
    {
        $rules = [
            'name' => 'required',
            'content_type' => 'required',
            'content_value' => 'required',
            'candidate_id' => 'exists:candidates,id|required',
        ];
        $messages = [
            'name.required' => 'O nome do template é requerido.',
            'content_type.required' => 'O tipo de conteúdo é requerido.',
            'content_value.required' => 'O candidato é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe',
            'candidate_id.required' => 'O candidato é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $templateEmailMarketing->fill($request->all());
        $templateEmailMarketing->content_value = htmlspecialchars($templateEmailMarketing->content_value);
        $templateEmailMarketing->save();
        return response()->json(['error' => 0, "msg" => "Template criado.", "data" => $templateEmailMarketing], 201);
    }
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $templateEmailMarketing = TemplateEmailMarketing::find($id);
        if (!$templateEmailMarketing) {
            return response()->json(['error' => 1, "msg" => "O template informado não foi encontrado", "data" => ""], 400);
        }
        $rules = [
            'name' => 'required',
            'content_type' => 'required',
            'content_value' => 'required',
            'candidate_id' => 'exists:candidates,id|required',
        ];
        $messages = [
            'name.required' => 'O nome do template é requerido.',
            'content_type.required' => 'O tipo de conteúdo é requerido.',
            'content_value.required' => 'O candidato é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe',
            'candidate_id.required' => 'O candidato é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $templateEmailMarketing->fill($request->all());
        $templateEmailMarketing->content_value = htmlspecialchars($templateEmailMarketing->content_value);
        $templateEmailMarketing->save();
        return response()->json(['error' => 0, "msg" => "Template atualizado.", "data" => $templateEmailMarketing], 200);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $templateEmailMarketing = TemplateEmailMarketing::find($id);
        if (!$templateEmailMarketing)
            return response()->json(['error' => 1, "msg" => "O template informado não foi encontrado", "data" => ""], 400);
        return response()->json(['error' => 0, "msg" => "", "data" => $templateEmailMarketing], 200);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        $templateEmailMarketing = TemplateEmailMarketing::find($id);
        if (!$templateEmailMarketing)
            return response()->json(['error' => 1, "msg" => "O template informado não foi encontrado", "data" => ""], 400);
        $templateEmailMarketing->delete();
        return response()->json(['error' => 0, "msg" => "O template foi removido", "data" => $templateEmailMarketing], 200);
    }
}
