<?php

namespace App\Http\Controllers;

use App\Http\Requests\StageCustomRequest;
use App\Repositories\StageRepository;
use Illuminate\Http\Request;

class StageController extends Controller
{
    private $repository;

    public function __construct(StageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function missingMethod($params = array())
    {
        $error = "Error 404";
        return response()->json($error, 404);
    }

    public function index (  )
    {
        $assets = $this->repository->paginate(10);
        return $assets;
    }

    public function store ( StageCustomRequest $request )
    {
        $data = $request->all();
        $this->repository->create($data);
        return $data;
    }

    public function show($id)
    {
        $item = $this->repository->find($id);
        return $item;
    }

    public function update(StageCustomRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return $data;
    }


    public function destroy( $id)
    {
        $this->repository->delete($id);
        $ok = "Deleted with sucess";
        return response()->json($ok, 200);
    }
}
