<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;

class CityController extends Controller
{
    /**
     * Retorna todas as cidades cadastradas ou as cidades de um estado especifico.
     * Para retornar as cidades de um estado, basta passar o parametro state com o ID do estado.
     * Exemplo: http://127.0.0.1:8000/api/cities?state=20
     */
    public function index(Request $request)
    {
        if($request->input('state')){
            return City::where('state_id', $request->input('state'))->get();
        }

        return City::all();
    }

    /**
     * Metodo para criar uma nova cidade.
     */
    public function store(Request $request)
    {
        $city = City::create($request->all());
        return response()->json($city, 201);
    }

    /**
     * Metodo para mostrar uma cidade de um ID especifico.
     */
    public function show(City $city)
    {
        return $city;
    }


    /**
     * Metodo para atualizar cidade selecionando um ID especifico.
     */
    public function update(Request $request, City $city)
    {
        $city->fill($request->all());
        $city->save();
        return response()->json($city);
    }

    /**
     * Metodo para remover cidade selecionando um ID especifico.
     */
    public function destroy(City $city)
    {
        $city->delete();
        return response()->json($city, 204);
    }
}
