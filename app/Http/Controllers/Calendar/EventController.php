<?php
namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use \App\Event;
use JWTAuth;
use \Symfony\Component\HttpFoundation\Request;


class EventController extends Controller
{
    /**
     * Retorna a agenda de um usuário especifico ou do usuário atual logado
     * @param Request $request | id = caso não seja informando utilizará do usuário logado
     * | month = caso não seja informado será feito um filtro completo do ano
     * | year = caso não seja informado será feito um filtro para o ano corrente
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $userCurrent = null;
        if (!$request->has('id')) {
            $userCurrent = JWTAuth::parseToken()->authenticate();
        } else {
            $userCurrent = User::find($request->get('id'));
            if (empty($userCurrent)) {
                return response()->json(['error' => 1, "msg" => "Usuário não encontrado", "data" => ""]);
            }
        }
        $Month = $request->has('month') ? $request->get('month') : null;
        $Year = $request->has('year') ? $request->get('year') : date('Y');
        $Calendar = Event::where('user_id', '=', $userCurrent->id);
        if (!empty($Month)) {
            $Calendar = $Calendar->whereMonth('start', $Month)->whereYear('start', $Year);
        } else {
            $Calendar = $Calendar->whereYear('start', $Year);
        }
        $Calendar = $Calendar->get();
        $Holidays = $this->getHolidays();
        $result = ['error' => 0, "msg" => "", "data" => ['Calendar' => $Calendar, 'Holidays' => $Holidays]];
        return response()->json($result);
    }

    /**
     * Novo evento na agenda.
     * Pode ser informado o ID de um politico para inserir em sua agenda
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'start' => 'required|date',
            'end' => 'required|date',
            'title' => 'max:100',
            'where' => 'max:100',
            'description' => 'max:1000',
            'type_events_id' => 'required|exists:type_events,id',
            'visibilite_events_id' => 'required|exists:visibilite_events,id'
        ];
        $messages = [
            'start.required' => 'A data inícial do evento é requerida.',
            'start.date' => 'A data de início está fora do padrão americano.',
            'end.required' => 'A data final do evento é requerida.',
            'title.max' => 'O tamanho máximo do titúlo é de 100 caracteres.',
            'where.max' => "O tamanho para campo onde é de  100 caracteres.",
            'description.max' => "O tamanho para campo descrição é de  1000 caracteres.",
            'type_events_id.required' => "O tipo de evento é requerido",
            'type_events_id.exists' => "O tipo de evento não existe",
            'visibilite_events_id.required' => "A visibilidade do evento é requerida",
            'visibilite_events_id.exists' => "A visibilidade do evento não existe",
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        /**
         * Verifica usuário
         */
        $userCurrent = null;
        if (!$request->has('user_id')) {
            $userCurrent = JWTAuth::parseToken()->authenticate();
        } else {
            $userCurrent = User::find($request->get('user_id'));
        }

        $validator->after(function ($validator) use ($userCurrent) {
            if (empty($userCurrent)) {
                $validator->errors()->add('user_id', 'Usuário não encontrado');
            }
        });
        /**
         * Verificar disponibilidade
         */
        $checkViability = Event::where('user_id', '=', $userCurrent->id)
            ->whereNotBetween('start', [$request->start, $request->end])
            ->whereNotBetween('end', [$request->start, $request->end])
            ->count();

        $validator->after(function ($validator) use ($checkViability, $request) {
            if (!$checkViability) {
                $validator->errors()->add('start', 'A data não está disponível');
            }
            if ($request->start > $request->end) {
                $validator->errors()->add('start', 'A data não inícial não pode ser menor que a final');
            }

        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        $event = new Event();
        $event->user_id = $userCurrent->id;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->where = $request->where;
        $event->type_events_id = $request->type_events_id;
        $event->visibilite_events_id = $request->visibilite_events_id;
        $event->save();
        return response()->json(['error' => 0, "msg" => "Evento inserido com sucesso.", "data" => $event]);
    }
    /**
     * Atualiza os eventos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'required:exists:events',
            'start' => 'required|date',
            'end' => 'required|date',
            'title' => 'max:100',
            'where' => 'max:100',
            'description' => 'max:1000',
            'type_events_id' => 'required|exists:type_events,id',
            'visibilite_events_id' => 'required|exists:visibilite_events,id'
        ];
        $messages = [
            'id.required' => 'O id do evento e requerido',
            'id.exists' => 'O id do evento mão existe',
            'start.required' => 'A data inícial do evento é requerida.',
            'start.date' => 'A data de início está fora do padrão americano.',
            'end.required' => 'A data final do evento é requerida.',
            'title.max' => 'O tamanho máximo do titúlo é de 100 caracteres.',
            'where.max' => "O tamanho para campo onde é de  100 caracteres.",
            'description.max' => "O tamanho para campo descrição é de  1000 caracteres.",
            'type_events_id.required' => "O tipo de evento é requerido",
            'type_events_id.exists' => "O tipo de evento não existe",
            'visibilite_events_id.required' => "A visibilidade do evento é requerida",
            'visibilite_events_id.exists' => "A visibilidade do evento não existe",
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($request) {
            if ($request->start > $request->end) {
                $validator->errors()->add('start', 'A data não inícial não pode ser menor que a final');
            }
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()]);
        }
        /**
         * Verificar disponibilidade
         */
        $event = Event::find($request->id);
        if(empty($event))
            return response()->json(['error' => 1, "msg" => "Evento não encontrado", "data" => null]);
        if($event->start != $request->start || $event->end != $request->end){
            $checkViability = Event::where('user_id', '=', $event->user_id)
                ->whereNotBetween('start', [$request->start, $request->end])
                ->whereNotBetween('end', [$request->start, $request->end]);
            if(!$checkViability->count()){
                $checkViability = Event::where('user_id', '=', $event->user_id)
                    ->whereNotBetween('start', [$request->start, $request->end])->get();
                if(empty($checkViability->count())){
                    return response()->json(['error' => 1, "msg" => "Data não está disponível", "data" => null]);
                }
                if($checkViability[0]->id != $event->id){
                    return response()->json(['error' => 1, "msg" => "Data não está disponível", "data" => null]);
                }
            }
        }
        $event->start = $request->start;
        $event->end = $request->end;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->where = $request->where;
        $event->type_events_id = $request->type_events_id;
        $event->visibilite_events_id = $request->visibilite_events_id;
        $event->save();
        return response()->json(['error' => 0, "msg" => "Evento Atualizado com sucesso", "data" => $event]);

    }
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){
        if(!is_numeric($id)){
            return response()->json(['error' => 1, "msg" => "Id fora do padrão", "data" => ""]);
        }

        $event = Event::find($id);
        if(empty($event)){
            return response()->json(['error' => 1, "msg" => "O evento não foi encontrado", "data" => ""]);
        }
        $event->delete();
        return response()->json(['error' => 0, "msg" => "O evento foi removido", "data" => ""]);

    }
    /**
     * Retorna todos os feriandos nacionais no ano corrente
     * @param int $year
     * @param bool $json
     * @return array|string
     */
    private function getHolidays($year = null, $json = false)
    {
        $year = empty($year) ? date('Y') : $year;
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'http://www.calendario.com.br/api/api_feriados.php?ano=' . $year . '&&token=d2VsbGluZ3RvbmZkc0BnbWFpbC5jb20maGFzaD0zNzQ1MTAzMQ==');
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml; charset=ISO-8859-1"));
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        $XmlConvert = simplexml_load_string($data);
        if ($json)
            return json_encode($XmlConvert);
        return (array)$XmlConvert;
    }
}