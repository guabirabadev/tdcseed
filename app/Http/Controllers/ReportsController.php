<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;

class ReportsController extends Controller
{
  public function reachPosts()
  {
    $chart = Charts::database(FacebookPosts::all(), 'line', 'highcharts')
          ->title('Alcance dos posts')
          ->elementLabel("Visualizacoes")
          ->responsive(true)
          ->groupBy('status', null, [0 => 'Like', 1 => 'Deslike']);
  }

  public function reachPosts()
  {
    $chart = Charts::database(FacebookPosts::all(), 'line', 'highcharts')
          ->title('Reacoes aos posts')
          ->elementLabel("Reacoes")
          ->responsive(true)
          ->groupBy('status', null, [0 => 'Like', 1 => 'Amei', 2 => 'Haha', 3 => 'Uau', 4 => 'Triste', 5 => 'Grr']);
  }
}
