<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\ContactMarketing;
use App\ListMarketing;
use Illuminate\Http\Request;

class ListMarketingController extends Controller
{
    public function store(Request $request,ListMarketing $listMarketing){
        $rules = [
            'name' => 'required',
            'candidate_id' => 'exists:candidates,id|required',
        ];
        $messages = [
            'name.required' => 'O nome da lista é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($request) {
             $checkName = ListMarketing::where('name','=',$request->get('name'))
                ->where('candidate_id','=',$request->get('candidate_id'))
                ->count();
            if($checkName){
                $validator->errors()->add('name', 'O candidato já possue uma lista com esse nome.');
            }
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $listMarketing->fill($request->all());
        $listMarketing->save();
        return response()->json(['error' => 0, "msg" => "", "data" => $listMarketing], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $listMarketing = ListMarketing::find($id);
        if(!$listMarketing)
            return response()->json(['error' => 1, "msg" => "Lista não encontrada", "data" => $id], 400);
        $listMarketing->delete();
        return response()->json(['error' => 0, "msg" => "Lista removida", "data" => $id], 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id){
        $listMarketing = ListMarketing::find($id);
        if(!$listMarketing)
            return response()->json(['error' => 1, "msg" => "Lista não encontrada.", "data" => $id], 400);

        $rules = [
            'name' => 'required',
            'candidate_id' => 'exists:candidates,id|required',
        ];
        $messages = [
            'name.required' => 'O nome da lista é requerido.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($request) {
            $checkName = ListMarketing::where('name','=',$request->get('name'))
                ->where('candidate_id','=',$request->get('candidate_id'))
                ->count();
            if($checkName){
                $validator->errors()->add('name', 'O candidato já possue uma lista com esse nome.');
            }
        });
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $listMarketing->fill($request->all());
        $listMarketing->save();
        return response()->json(['error' => 0, "msg" => "", "data" => $listMarketing], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {   $listMarketing = ListMarketing::find($id);
        if(!$listMarketing)
            return response()->json(['error' => 1, "msg" => "Lista não encotrada", "data" => ""], 400);
        return response()->json(['error' => 0, "msg" => "", "data" => $listMarketing], 200);
    }

    /**
     * Retorna todas a lista do candidato informado
     * @param Candidate $candidate
     * @return \Illuminate\Http\JsonResponse
     */
    public function listForCandidate(Candidate $candidate){
        return response()->json(['error' => 0, "msg" => "", "data" => $candidate->listMarketing()->get()], 200);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeContact(Request $request){
        $rules = [
            'contact_id' => 'required|exists:contact_marketing,id',
            'list_id' => 'required|exists:list_marketing,id',
        ];
        $messages = [
            'contact_id.required' => 'O contato é requerido',
            'contact_id.exists' => 'O contato informado não existe.',
            'list_id.exists' => 'A lista informada não existe.',
            'list_id.required' => 'A lista é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }

        $contact = ContactMarketing::find($request->get('contact_id'));
        $list = ListMarketing::find($request->get('list_id'));
        if($list->hasContact($contact)){
            return response()->json(['error' => 1, "msg" => "O contato já está adicionado a lista", "data" => null], 400);
        }
        if($list->candidate_id != $contact->candidate_id){
            return response()->json(['error' => 1, "msg" => "O contato e a lista são de candidatos diferentes", "data" => null], 400);
        }
        $list->contacts()->attach($contact);
        return response()->json(['error' => 0, "msg" => "O contato foi adicionado a lista", "data" => null], 201);
    }

    /**
     * @param $listMarketingId
     * @param $contactMarketingId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyContact($listMarketingId,$contactMarketingId){
        $listMarketing = ListMarketing::find($listMarketingId);
        if(!$listMarketing){
            return response()->json(['error' => 1, "msg" => "A lista informada não foi encontrada.", "data" => null], 400);
        }
        $contactMarketing = ContactMarketing::find($contactMarketingId);
        if(!$contactMarketing){
            return response()->json(['error' => 1, "msg" => "O contato informado não foi encontrado.", "data" => null], 400);
        }
        if(!$listMarketing->hasContact($contactMarketing)){
            return response()->json(['error' => 1, "msg" => "A lista informanda não tem o contato", "data" => null], 400);
        }
        $listMarketing->contacts()->detach($contactMarketing);
        return response()->json(['error' => 0, "msg" => "O contato foi removido da lista", "data" => null], 200);
    }





}
