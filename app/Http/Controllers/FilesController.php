<?php

namespace App\Http\Controllers;

use App\Models\File as ModelFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return ModelFile::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $file = $request->file('file');
        $treatment = new \App\Helpers\Files( $request->file('file') );

        if( $file->move( $treatment->file_dir, $treatment->file_disk ) ) {
            $data = ModelFile::create( $treatment->getFileData() );
        }

        return ( isset( $data ) ? response()->json( $data, 201 ) : null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $data = ModelFile::find( $id );
        return response()
            ->json( $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $file = ModelFile::find( $id );
        $file->fill( $request->all() );
        $file->save();
        return response()
            ->json($file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $file = ModelFile::find( $id );
        $file->delete();
        return response()
            ->json( [], 204 );
    }
}
