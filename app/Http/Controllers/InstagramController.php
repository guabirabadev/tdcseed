<?php

namespace App\Http\Controllers;

use App\Repositories\InstagramRepository;
use Illuminate\Http\Request;
use InstagramAPI\Instagram;


class InstagramController extends Controller
{

    //temos que pensar em algo que resolva o problema das multiplas autenticaçoes distribuidas
    /**
     * @var InstagramRepository
     */
    private $repository;

    public function __construct( InstagramRepository $repository)
    {
        $this->repository = $repository;
    }



    public function login(Request $request)
    {
        $username = 'iministerialpdeus'; //$request['username']
        $password = 'jesus333'; //$request['password']
        $debug = false;
        $truncatedDebug = false;

        $ig = new Instagram($debug, $truncatedDebug);

        try{
            $ig->setUser($username, $password);
            $p = $ig->login();

        }catch (\Exception $e) {
            echo 'Ocorreu um erro ao logar no instagram: '.$e->getMessage()."\n";
            exit(0);
        }
       // $user = $ig->getUsernameId('iministerialpdeus');
        return response()->json($ig, 200);
    }

    public function post_image(Request $request)
    {
        $data = $request->all();
        $file = $request->file('img');
        //dd($file);

        $username = 'iministerialpdeus'; //$request['username']
        $password = 'jesus333'; //$request['password']
        $debug = false;
        $truncatedDebug = false;
        $captionText = 'Jesus';

        $ig = new Instagram($debug, $truncatedDebug);

        try{
            $ig->setUser($username, $password);
            $p = $ig->login();

        }catch (\Exception $e) {
            echo 'Ocorreu um erro ao logar no instagram: '.$e->getMessage()."\n";
            exit(0);
        }

        try {
            $ig->uploadPhoto($file, $captionText); //bug resolvido
            return "sucesso!";

        } catch (\Exception $e) {
            echo 'Ocorreu o seguinte erro ao enviar a imagem: '.$e->getMessage()."\n";
            exit(0);
        }
    }

    public function post_album(Request $request)
    {
        $username = 'iministerialpdeus'; //$request['username']
        $password = 'jesus333'; //$request['password']
        $debug = false;
        $truncatedDebug = false;
        $captionText = '';

        $ig = new Instagram($debug, $truncatedDebug);

        try{
            $ig->setUser($username, $password);
            $p = $ig->login();

        }catch (\Exception $e) {
            echo 'Ocorreu um erro ao logar no instagram: '.$e->getMessage()."\n";
            exit(0);
        }
        $media = [ //Maximo de 10 fotos ou videos
            [
                'type'     => 'photo',
                'file'     => '',
            ],
            [
                'type'     => 'photo',
                'file'     => '',
            ],
            [
                'type'     => 'video',
                'file'     => '',
            ],
        ];

        $ig = new Instagram($debug, $truncatedDebug);
        try {
            $ig->setUser($username, $password);
            $ig->login();
        } catch (\Exception $e) {
            echo 'Something went wrong: '.$e->getMessage()."\n";
            exit(0);
        }
        try {
            $ig->uploadPhotoStory($request['media'], $request['caption']);
        } catch (\Exception $e) {
            echo 'O correu o seguinte erro ao postar o album: '.$e->getMessage()."\n";
        }
        
    }

    public function post_video(Request $request)
    {
        $username = 'iministerialpdeus'; //$request['username'];
        $password = 'jesus333'; //$request['password']
        $debug = false;
        $truncatedDebug = false;

        $ig = new Instagram($debug, $truncatedDebug);

        try{
            $ig->setUser($username, $password);
            $p = $ig->login();

        }catch (\Exception $e) {
            echo 'Ocorreu um erro ao logar no instagram: '.$e->getMessage()."\n";
            exit(0);
        }

        try {
            $ig->uploadVideo($request['video_name'], ['caption' => $request['caption']], 8);
        } catch (\Exception $e) {
            echo 'O correu o seguinte erro ao postar o video: '.$e->getMessage()."\n";
        }
    }
}
