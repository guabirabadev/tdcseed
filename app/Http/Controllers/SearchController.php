<?php

namespace App\Http\Controllers;

use App\Models\Voter;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @param $term
     * @param $filter
     * @return \Illuminate\Http\JsonResponse
     */
    public function voters( Request $request )
    {
        $term = $request->input('term');
        $filter = $request->input('filter');
        $order = explode('-', $request->input('order'));

//        switch ( $filter ) {
//            case 'genre':
//                $voters = Voter::genre( $term );
//                break;
//            case 'age':
//                $voters = Voter::age( $term );
//                break;
//            case 'education':
//                $voters = Voter::with(['education' => function( $query ) use ( $term ) {
//                    $query->where('education', $term)->get();
//                }]);
//                break;
//            case 'intentions':
//                $voters = Voter::with(['vote' => function( $query ) use ( $term ) {
//                    $query->where( 'type', $term );
//                }]);
//                break;
//            default:
//                /**
//                 * For multiple keys
//                 * birth-{mm, dd, yy}
//                 * address-{street, district, city, state}
//                 * zone-{zone, section, city, uf}
//                 */
//                $type = explode('-', $filter);
//                if( is_array( $type ) ) {
//                    switch ( $type[0] ) {
//                        case 'birth':
//                            $voters = Voter::birth( $term, $filter );
//                            break;
//                        case 'address':
//                            $voters = Voter::with(['addresses' => function( $query ) use ( $term, $type ) {
//                                $query->where( $type[1], $term );
//                            }]);
//                            break;
//                        case 'zone':
//                            $voters = Voter::with(['zones' => function( $query ) use ( $term, $type ) {
//                                $query->where( $type[1], $term );
//                            }]);
//                            break;
//                        default:
//                            $voters = null;
//                    }
//                }
//        }

        if( isset( $voters ) ) {
            $data = [
                'success' => true,
                'voters' => $voters->get()
            ];
        } else {
            $data = [
                'success' => false,
                'message' => 'Nenhum resultado encontrado.'
            ];
        }

        return response()
            ->json( $data, 201 );
    }

    /**
     * Search the products table.
     *
     * @param  Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        // First we define the error message we are going to show if no keywords
        // existed or if no results found.
        $error = ['error' => 'No results found, please try with different keywords.'];

        // Making sure the user entered a keyword.
        if($request->has('q')) {

            // Using the Laravel Scout syntax to search the products table.
            $posts = Voter::search($request->input('term'))->get();

            // If there are results return them, if none, return the error message.
            return $posts->count() ? $posts : $error;

        }

        // Return the error message if no keywords existed
        return $error;
    }
}
