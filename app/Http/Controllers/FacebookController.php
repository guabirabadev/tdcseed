<?php

namespace App\Http\Controllers;


use Facebook\Exceptions\FacebookSDKException;

use Facebook\Facebook;
use Facebook\Facebook\Exceptions\FacebookResponseException;
use Illuminate\Http\Request;


class FacebookController extends Controller
{

    public function index()
    {
        $fb = "Sucess";
        return response()->json($fb, 200);
    }

    public function callback()
    {
        if(!session_id()) {
            session_start();
        }

        $fb = $this->getConfig();
        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        echo '<h3>Tekin de acessu Token</h3>';
        var_dump($accessToken->getValue());
        $oAuth2Client = $fb->getOAuth2Client();
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        echo '<h3>Metadata</h3>';
        var_dump($tokenMetadata);

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                exit;
            }

            echo '<h3>Long-lived</h3>';
            var_dump($accessToken->getValue());
        }

        //return $accessToken->getValue();
        $_SESSION['fb_access_token'] = (string) $accessToken;

        return $token = $accessToken->getValue();

    }

    public function getAccessToken()
    {
        $fb = $this->getConfig();
        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $tk = $accessToken->getValue();
        dd($tk);


    }
    
    public function login()
    {
        if(!session_id()) {
            session_start();
        }
        $fb = $this->getConfig();

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://elejase.dev/api/facebook/callback', $permissions);

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Logar-se com o facebook</a>';
    }

    public function getConfig()
    {
        return $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'), // Replace {app-id} with your app id
            'app_secret' => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v2.8',
        ]);
    }

    public function getUserData()
    {
        $fb = $this->getConfig();
        $token = $this->callback();
        try {
            $response = $fb->get('/me?fields=id,name', $token);
        } catch(FacebookResponseException $e) {
            echo 'O OpenGraph retornou o seguinte erro: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'O Facebook: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        echo 'Name: ' . $user['name'];
    }

    public function getUser()
    {
        $fb = $this->getConfig();
        $access_toke = 'EAAEITGBoQEkBAFpxzFMAX7VXWNgnUnsjrjNZBju5jrq9cmGaYDxhJKNp0yJ2ZB3DnTcSdBYoZBX98nDb6m8HTCdjk0SDn3wSoGdjef6hKOk8DZCtJCCCpn4xqu4T7UrW2HjgkM4WZBOh4vBoGoF9su4uCzc5kaOampaEClIr1lgZDZD';
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name', $access_toke);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        echo 'Name: ' . $user['name'];
        echo '<br>ID: ' . $user['id'];
    }

    public function postPhoto(Request $request)
    {
        $fb = $this->getConfig();
        $access_toke = 'EAAEITGBoQEkBAFpxzFMAX7VXWNgnUnsjrjNZBju5jrq9cmGaYDxhJKNp0yJ2ZB3DnTcSdBYoZBX98nDb6m8HTCdjk0SDn3wSoGdjef6hKOk8DZCtJCCCpn4xqu4T7UrW2HjgkM4WZBOh4vBoGoF9su4uCzc5kaOampaEClIr1lgZDZD';
        $message = $request['msg'];
        $file = $request->file('img');
        $data = [
            'message' => $message,
            'source' => $fb->fileToUpload($file),
        ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->post('/me/photos', $data, $access_toke);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        echo 'Photo ID: ' . $graphNode['id'];
    }
}
