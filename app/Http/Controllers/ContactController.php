<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Metodo para mostrar todos os registros.
     */
    public function index()
    {
        return Contact::all();
    }

    /**
     * Metodo para criar um novo registro.
     */
    public function store(Request $request)
    {
        $contact = Contact::create($request->all());
        return response()->json($contact, 201);
    }

    /**
     * Metodo para mostrar um registro selecionando um ID especifico.
     */
    public function show(Contact $contact)
    {
       return $contact;
    }


    /**
     * Metodo para atualizar registro selecionando um ID especifico.
     */
    public function update(Request $request, Contact $contact)
    {
        $contact->fill($request->all());
        $contact->save();
        return response()->json($contact);
    }

    /**
     * Metodo para remover registro selecionando um ID especifico.
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return response()->json($contact, 204);
    }
}
