<?php

namespace App\Http\Controllers;

use App\ContactMarketing;
use App\Jobs\RenderTemplateMarketing;
use App\ListMarketing;
use App\Plan;
use Illuminate\Http\Request;

class ContactsMarketingController extends Controller
{
    /**
     * @param Request $request
     * @param ContactMarketing $contactMarketing
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request,ContactMarketing $contactMarketing){
        $rules = [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'custom_fields' => 'json',
            'candidate_id' => 'exists:candidates,id|required',
            'list_id'=>'exists:list_marketing,id'
        ];
        $messages = [
            'email.required' => 'O email é requerido.',
            'email.email' => 'O email não é válido.',
            'first_name.required' => 'O primeiro nome é requerido.',
            'last_name.required' => 'O último nome é requerido.',
            'custom_fields.json' => 'O campo customizado não é um json.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
            'list_id.exists' => 'A lista informada não existe.'
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->after(function ($validator) use ($request) {
            $checkEmail = ContactMarketing::where('email','=',$request->get('email'))
                ->where('candidate_id','=',$request->get('candidate_id'))
                ->count();
            if($checkEmail){
                $validator->errors()->add('email', 'O e-mail já está cadastrado para o candidato.');
            }
        });

        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $contactMarketing->fill($request->all());
        $contactMarketing->save();
        if($request->has('list_id')){
            $listMarketing = ListMarketing::find($request->get('list_id'));
            $listMarketing->contacts()->attach($contactMarketing);
        }
        return response()->json(['error' => 0, "msg" => "", "data" => $contactMarketing], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $contactMarketing = ContactMarketing::find($id);
        if(!$contactMarketing)
            return response()->json(['error' => 1, "msg" => "O contato não foi encontrado.", "data" => $id], 400);
        $contactMarketing->delete();
        return response()->json(['error' => 0, "msg" => "O contato foi removido", "data" => $contactMarketing], 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id){
        $contactMarketing = ContactMarketing::find($id);
        if(!$contactMarketing)
            return response()->json(['error' => 1, "msg" => "O contato não foi encontrado.", "data" => $id], 400);
        $rules = [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'custom_fields' => 'json',
            'candidate_id' => 'exists:candidates,id|required',
        ];
        $messages = [
            'email.required' => 'O email é requerido.',
            'email.email' => 'O email não é válido.',
            'first_name.required' => 'O primeiro nome é requerido.',
            'last_name.required' => 'O último nome é requerido.',
            'custom_fields.json' => 'O campo customizado não é um json.',
            'candidate_id.exists' => 'O candidato informado não existe.',
            'candidate_id.required' => 'O candidato é requerido.',
        ];
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['error' => 1, "msg" => "", "data" => $validator->errors()], 400);
        }
        $validator->after(function ($validator) use ($request,$contactMarketing) {
            $checkEmail = ContactMarketing::where('email','=',$request->get('email'))
                ->where('candidate_id','=',$request->get('candidate_id'))
                ->count();
            if($checkEmail){
                $validator->errors()->add('email', 'O e-mail já está cadastrado para o candidato.');
            }
            if($contactMarketing->candidate_id != $request->get('candidate_id')){
                $validator->errors()->add('candidate_id', 'O id do candidato está diferente desde quando o contato foi criado.');
            }

        });
        $contactMarketing->fill($request->all());
        $contactMarketing->save();


        return response()->json(['error' => 0, "msg" => "", "data" => $contactMarketing], 201);
    }

    public function show($id)
    {   $contactMarketing = ContactMarketing::find($id);
        if(!$contactMarketing)
            return response()->json(['error' => 1, "msg" => "Contato não encotrado", "data" => ""], 400);
        return response()->json(['error' => 0, "msg" => "", "data" => $contactMarketing], 200);
    }

}
