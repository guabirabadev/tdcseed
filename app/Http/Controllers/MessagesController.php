<?php

namespace App\Http\Controllers;


use App\Attachment;
use App\Messages;
use App\MessagesTo;
use App\Repositories\MessagesRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class MessagesController extends Controller
{
    /**
     * @var MessagesRepository
     */
    private $messagesRepository;

    public function __construct ( MessagesRepository $messagesRepository )
    {
        //metodo construtor
        $this->messagesRepository = $messagesRepository;
    }

    public function index (  )
    {
        //retorna todas as mensagens (acho que esse metodo ira ser extinto, ja que o seu conteudo é 'privado')
        $messages = $this->messagesRepository->paginate(15);
        return json_encode($messages);
    }

    public function store (Request $request)
    {
        //captura o ID do usuario que eta logado no momento {{ isso vai para o USER_SEND }}
        //$users = auth()->user()->id;


       $aux = Messages::create([
           'user_send' => $request->user_send,
           'title' => $request->title,
           'body' => $request->body
        ]);

        //envia a mensagem
        MessagesTo::create([
            'message_id' => $aux->id,
            'user_to' => 1,
        ]);

        //salva os anexos
        if ($request->hasFile('fileUpload')) {
            $name = $request->file('fileUpload')->getClientOriginalName();
            $ext = $request->file('fileUpload')->extension();
            $path = Storage::putFile('uploads', new File($request->file('fileUpload')));
            $encoding = base64_encode($request->file('fileUpload'));
        Attachment::create([
            'message_id'  => $aux->id,
            'name' => $name,
            'type' => $ext,
            'path' => $path
        ]);
        }

        return json_encode($aux);
    }

    public function show ($id)
    {
        $message = $this->messagesRepository->find($id);
        return json_encode($message);
    }

    public function destroy ($id)
    {
        $this->messagesRepository->delete($id);
        return "deletado com sucesso";
    }
}
