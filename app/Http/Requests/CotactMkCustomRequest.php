<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CotactMkCustomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profileAvatar' => 'required|dimensions:min_width=100,min_height=200',
            'title' => 'required',
            'firistName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'company_id' => 'required',
            'addressLineOne' => 'required',
            'addressLineTwo',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'attribuition' => 'required',
            'mobile' => 'required',
            'preferedLocale'  => 'required',
            'timeZone' => 'required', //vamos precisar dessa informação para obter alocalização do contato
            'twitter',
            'facebook',
            'googleplus',
            'skype',
            'instagram',
            'linkedin'
            //devo adicionar as midias sociais do contato como obrigatorias?
        ];
    }
}
