<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name' => 'required|Between:3,254',
            'name_political' => 'required|Between:3,254',
            'email' => 'required|email|unique:users|Between:6,64',
            'telephone' => 'required|Between:8,22',
            'address' => 'required|min:3'
        ];
    }
}
