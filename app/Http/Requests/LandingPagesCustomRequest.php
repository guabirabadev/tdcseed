<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LandingPagesCustomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tempalte_id',
            'title' => 'required',
            'alias'  => 'required',
            'category_id'  => 'required',
            'languagem'  => 'required',
            'published' => 'required',
            'publishAt' => 'required',
            'unPublishAt' => 'required',
            'redirectType'  => 'required',
            'meta' => 'required'
        ];
    }
}
