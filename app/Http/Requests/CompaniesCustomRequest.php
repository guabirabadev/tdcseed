<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompaniesCustomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email'=> 'required',
            'addressLineOne'=> 'required|min:3',
            'city' => 'required|min:3',
            'state' => 'required',
            'zipcode' => 'required',
            'country' => 'required',
            'phone' => 'required',
            'owner' => 'required',
            'numberOfEmployees',
            'industry',
            'description'  => 'required',
            'score'  => 'required'
        ];
    }
}
