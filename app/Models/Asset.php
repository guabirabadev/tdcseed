<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Asset extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'path' => 'required',
        'title' => 'required|min:3',
        'alias' => 'required',
        'description' => 'required|min:3',
        'category_id' => 'required',
        'languagem' => 'required',
        'published' => 'required',
        'publishAt' => '',
        'unPublishAt'
    ];

}
