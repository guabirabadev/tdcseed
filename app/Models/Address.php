<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street', 'number', 'district', 'city', 'state', 'complement', 'reference'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     * 
     * removi o id do Hidden
     */
    protected $hidden = [
        'created_at', 'updated_at', 'user_id'
    ];

}
