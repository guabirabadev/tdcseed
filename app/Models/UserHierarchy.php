<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHierarchy extends Model
{
    protected $table = 'users_hierarchies';

    protected $fillable = ['user_id', 'user_father'];

    /**
     * Get the user father.
     */
    public function father()
    {
        return $this->hasOne(User::class);
    }
}
