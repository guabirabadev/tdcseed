<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class File extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'file_disk',
        'file_path',
        'file_url',
        'file_ext',
        'file_size',
        'file_mime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'attach_id',
        'attach_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function attach()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getFileUrlAttribute()
    {
        return url('images') . "/{$this->attributes['file_url']}";
    }

    /**
     * @return string
     */
    public function getFilePathAttribute()
    {
        return Config::get('filesystems.disks.uploads.root') . $this->attributes['file_path'];
    }
}
