<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Interactions extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    'product_name',
    'product_group',
    'product_type',
    'product_unity_price',
    'product_group_price',
    'sender_id',
    'sender_name',
    'serder_email',
    'message',
    'key_foy_api'
    ];

}
