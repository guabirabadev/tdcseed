<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Voter extends Model
{
    use Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname',
        'genre',
        'birth',
        'observations'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'scholarity', 'age', 'image' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'education'
    ];

    /**
     * @return string
     */
    public function getGenreAttribute()
    {
        return ( $this->attributes['genre'] ? 'Masculino' : 'Feminino' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function education()
    {
        return $this->morphMany(Education::class, 'education');
    }

    /**
     * @return mixed
     */
    public function getScholarityAttribute()
    {
        $result = $this->education()->first();
        return (isset($result['education']) ? $result['education'] : 'N/A');
    }

    /**
     * @return string
     */
    public function getAgeAttribute()
    {
        $birth = $this->attributes['birth'];
        $now = Carbon::now();
        $parse = Carbon::parse($birth);
        $age = Carbon::createFromDate($parse->year, $parse->month, $parse->day)->diff( $now )->format('%y');
        return $age;
    }

    public function getBirthAttribute()
    {
        $birth = $this->attributes['birth'];
        $parse = Carbon::parse($birth);
        return Carbon::createFromDate($parse->year, $parse->month, $parse->day)->format('d/m/Y');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vote()
    {
        return $this->hasOne(Vote::class, 'voter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function image()
    {
        return $this->morphMany(File::class, 'attach');
    }

    /**
     * @return mixed
     */
    public function getImageAttribute()
    {
        $image = $this->image()->first();
        return (isset( $image['file_url'] ) ? $image['file_url'] : null);
    }

    /**
     * @param $query
     * @param $term
     * @param $filter
     * @return mixed
     */
    public function scopeBirth( $query, $term, $filter )
    {
        $type = explode('-', $filter);
        switch ( $type[1] ) {
            case 'mm':
                return $query->whereMonth('birth', '=', $term);
                break;
            case 'dd':
                return $query->whereDay('birth', '=', $term);
                break;
            default:
                return $query->whereYear('birth', '=', $term);
                break;
        }
    }

    /**
     * @param $query
     * @param $genre
     * @return mixed
     */
    public function scopeGenre($query, $genre )
    {
        if( is_numeric( $genre ) ) {
            $result = $genre;
        } else {
            $result = ( $genre == 'masc' ? 1 : 0 );
        }

        return $query->where( 'genre', $result );
    }

    /**
     * @param $query
     * @param $term
     * @return mixed
     */
    public function scopeAge( $query, $term )
    {
        $now = Carbon::now();
        $parse = $now->subYears( $term );
        return $query->whereYear( 'birth', $parse->year );
    }

}
