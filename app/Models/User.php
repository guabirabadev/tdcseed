<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $appends = [
        'hierarchy', 'data', 'contact', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot(){
        parent::boot();

        static::creating(function ($user){
            $user->token = str_random(40);
        });
    }

    public function hasVerified(){
        $this->verified = true;
        $this->token = null;
        $this->save();
    }

    /**
     * Get the users on hierarchy
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hierarchy()
    {
        return $this->hasMany(UserHierarchy::class, 'user_father')
            ->select([
                'user_id'
            ]);
    }

    /**
     * Returns only the child IDs.
     * @return array|null
     */
    public function getHierarchyAttribute()
    {
        $users = $this->hierarchy()->get();
        foreach( $users as $user ) {
            $arr[] = $this->where('id', $user['user_id'])->first();
        }

        return (isset($arr) ? $arr : null);
    }

    /**
     * Check if current user is father on action
     * USAGE: \Auth::user()->isFather( $id ) - $id of user, for actions as update, delete and others.
     * @param  int $id
     * @return mixed
     */
    public function isFather( $id )
    {
        return (UserHierarchy::where('user_father', $this->id)
            ->where('user_id', $id)
            ->count() ? true : false);
    }

    /**
     * @return mixed
     */
    public function getDataAttribute()
    {
        return $this->data()
            ->first();
    }

    /**
     * @return mixed
     */
    public function getContactAttribute()
    {
        return $this
            ->contact()
            ->first();
    }

    /**
     * @return mixed
     */
    public function getAddressAttribute()
    {
        return $this
            ->address()
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        $type = $this->attributes['type'];

        switch ( $type ):
            case 'candidate':
                return $this->hasOne(Candidate::class, 'user_id');
            default:
                return $this->hasOne(Profile::class, 'user_id');
                break;
        endswitch;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this->hasOne(Contact::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function zone()
    {
        return $this->morphMany(Zone::class, 'zone');
    }
}
