<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'educations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'education'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at', 'education_id', 'education_type'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function education()
    {
        return $this->morphTo();
    }

    /**
     * @return mixed
     */
    public function getEducationAttribute()
    {
        $educations = config( 'system.educations' );
        $education = $educations[$this->attributes['education']];
        return (isset($education) ? $education : 'N/A');
    }
}
