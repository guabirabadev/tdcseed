<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ContactMk extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'profileAvatar',
        'title',
        'firistName',
        'lastName',
        'email',
        'company_id',
        'addressLineOne',
        'addressLineTwo',
        'city',
        'state',
        'country',
        'attribuition',
        'attribuitionDate',
        'phone',
        'mobile',
        'fax',
        'website',
        'preferedLocale',
        'timeZone',
        'stage',
        'contactOwner',
        'tags',
        'twitter',
        'facebook',
        'googleplus',
        'skype',
        'instagram',
        'linkedin'
    ];

}
