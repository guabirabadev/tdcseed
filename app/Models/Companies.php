<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Companies extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name',
        'email',
        'addressLineOne',
        'addressLineTwo',
        'city',
        'state',
        'zipcode',
        'country',
        'website',
        'phone',
        'owner',
        'annualRevenue',
        'fax',
        'numberOfEmployees',
        'industry',
        'description',
        'score'
    ];

}
