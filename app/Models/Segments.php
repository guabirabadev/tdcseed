<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Segments extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name',
        'alias',
        'description',
        'publicSegment',
        'published'
    ];

}
