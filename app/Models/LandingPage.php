<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LandingPage extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'tempalte_id',
        'title',
        'alias',
        'category_id',
        'languagem',
        'published',
        'publishAt',
        'unPublishAt',
        'redirectType',
        'meta'
    ];

}
