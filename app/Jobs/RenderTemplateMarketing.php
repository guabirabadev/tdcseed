<?php

namespace App\Jobs;

use App\CampaignMarketing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class RenderTemplateMarketing implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $campaignMarketing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CampaignMarketing $campaignMarketing)
    {
        $this->campaignMarketing = $campaignMarketing;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    }
}

