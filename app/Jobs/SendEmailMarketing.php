<?php

namespace App\Jobs;

//<<<<<<< HEAD
//=======
use App\CampaignMarketing;
use Carbon\Carbon;
//>>>>>>> origin/dev/#28-email_marketing
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
//<<<<<<< HEAD
//=======
use Illuminate\Support\Facades\Storage;
//>>>>>>> origin/dev/#28-email_marketing

class SendEmailMarketing implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
//<<<<<<< HEAD
    public function __construct()
    {
        //
//=======

    private $campaignMarketing;
    public function __construct(CampaignMarketing $campaignMarketing)
    {
        $this->campaignMarketing = $campaignMarketing;
//>>>>>>> origin/dev/#28-email_marketing
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//<<<<<<< HEAD
        //
//=======
        $this->send($this->campaignMarketing);
        $this->campaignMarketing->last_send = Carbon::now();
        $this->campaignMarketing->save();
    }

    public function send(CampaignMarketing $campaign)
    {

        $senders = $campaign->sender;
        $template = $campaign->templateEmailMarketing;
        $contacts = $campaign->listContacts->contacts;
        $personalizations['personalizations'] = [];
        $personalization = ['to'=>[],'subject'=>""];
        $to['to'] = [];
        $i = 0;
        foreach ($contacts as $contact):
            array_push($to['to'], ['email' => $contact->email,'name' => $contact->first_name . ' ' . $contact->last_name]);
            if($i==10)
                break;
            $i++;
        endforeach;
        $personalization['subject'] = $campaign->name;
        $personalization['to'] = $to['to'];
        array_push($personalizations['personalizations'],$personalization);

        $personalizations['from'] = ['email' => $senders->from, 'name' => $senders->from_name];
        $personalizations['content'] = [[
            'type' => 'text/plain',
            'value' => Storage::disk('public')->get(env('RENDER_EMAIL_MARKETING') . '/' . md5($template->id) . ".html")
        ]];
        $personalizations['tracking_settings'] = [
            'click_tracking' => [
                'enable'=>true,
                'enable_text'=>true

            ],
            'open_tracking' => [
                'enable'=>true
            ],
            'subscription_tracking'=>[
                'enable'=>true
            ],
            'ganalytics'=>[
                'enable'=>true
            ]
        ];
        //echo print_r($AUX);
        //echo json_encode($personalizations);
        //exit();
        $sg = new \SendGrid(env('SENDGRID_API_KEY'));
        $response = $sg->client->mail()->send()->post($personalizations);
        echo $response->statusCode();
        print_r($response->headers());
        echo $response->body();


//>>>>>>> origin/dev/#28-email_marketing
    }
}
