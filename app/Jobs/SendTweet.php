<?php

namespace App\Jobs;

use App\Repositories\TweetScheduleRepository;
use App\Tweet;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Thujohn\Twitter\Facades\Twitter;

class SendTweet implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Tweet
     */
    private $tweet;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Tweet $tweet)
    {
        $this->tweet = $tweet;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TweetScheduleRepository $repository, $id, Twitter $twitter)
    {
        $repository->update($id);//atualiza nos agendamentos
        $tw = $repository->find($id);
        $twitter->postTweet($id);
    }
}
