<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MessagesTo extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'message_id',
        'user_to',
        'readed',
        'readed_at'
    ];

}
