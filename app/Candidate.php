<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidates';
    protected $fillable = [
        'user_id',
        'name',
        'name_political',
        'email',
        'telephone',
        'address',
        'plan_id'
    ];

    /**
     * Lista todos os planos contratados do candidato
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans()
    {
        return $this->belongsToMany(\App\Plan::class);
    }

    /**
     * Retonar todas as faturas do plano contratado
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidatePlans()
    {
        return $this->hasMany(CandidatePlan::class);

    }
    public function getCandidatePlan(Plan $plan){
        foreach ($this->candidatePlans as $candidatePlan){
            if($candidatePlan->plan_id == $plan->id){
                return $candidatePlan;
            }
        }
        return null;
    }

    /**
     * Retonar a fatura com o menor consumo
     * @param $smsConsume integer
     * @param $emailConsume integer
     * @return PlanConsume
     */
    public function smallerConsume($smsConsume = 0, $emailConsume = 0)
    {
        /**
         * priority
         * 1 - para priorizar sms
         * 2 - para prioriza  e-mail
         * 0 - para menor comsumos dos dois
         */
        $priority = 0;
        if ($smsConsume > 0 && $emailConsume == 0) {
            $priority = 1;
        }
        if ($emailConsume > 0 && $smsConsume == 1) {
            $priority = 2;
        }

        $candidatePlans = $this->hasMany(CandidatePlan::class)->get();
        $planMinConsume = null;
        foreach ($candidatePlans as $candidatePlan) {
            if (empty($planMinConsume))
                $planMinConsume = $candidatePlan->currentConsume();
            else {
                if ($priority == 1) {
                    if ($planMinConsume->sms_consume > $candidatePlan->currentConsume()->sms_consume)
                        $planMinConsume = $candidatePlan->currentConsume();
                } elseif ($priority == 2) {
                    if ($priority == 1) {
                        if ($planMinConsume->email_consume > $candidatePlan->currentConsume()->email_consume)
                            $planMinConsume = $candidatePlan->currentConsume();
                    }
                } else {
                    if ($planMinConsume->email_consume > $candidatePlan->currentConsume()->sms_email && $planMinConsume->sms_consume > $candidatePlan->currentConsume()->sms_consume)
                        $planMinConsume = $candidatePlan->currentConsume();
                }
            }
        }
        return $planMinConsume;

    }

    /**
     * Verifica se o candidato tem um plano específico
     * @param Plan $plan
     * @return bool
     */
    public function hasPlan(Plan $plan)
    {
        foreach ($this->plans as $planCandidate) {
            if ($planCandidate->id == $plan->id)
                return true;
        }
        return false;
    }
//    public function getCandidatePlan(Plan $plan){
//        foreach ($this->candidatePlans as $candidatePlan){
//            if($candidatePlan->plan_id == $plan->id){
//                return $candidatePlan;
//            }
//        }
//        return null;
//    }

    /**
     * Retonar a fatura com o menor consumo
     * @param $smsConsume integer
     * @param $emailConsume integer
     * @return PlanConsume
     */
//    public function smallerConsume($smsConsume = 0, $emailConsume = 0)
//    {
//        /**
//         * priority
//         * 1 - para priorizar sms
//         * 2 - para prioriza  e-mail
//         * 0 - para menor comsumos dos dois
//         */
//        $priority = 0;
//        if ($smsConsume > 0 && $emailConsume == 0) {
//            $priority = 1;
//        }
//        if ($emailConsume > 0 && $smsConsume == 1) {
//            $priority = 2;
//        }
//
//        $candidatePlans = $this->hasMany(CandidatePlan::class)->get();
//        $planMinConsume = null;
//        foreach ($candidatePlans as $candidatePlan) {
//            if (empty($planMinConsume))
//                $planMinConsume = $candidatePlan->currentConsume();
//            else {
//                if ($priority == 1) {
//                    if ($planMinConsume->sms_consume > $candidatePlan->currentConsume()->sms_consume)
//                        $planMinConsume = $candidatePlan->currentConsume();
//                } elseif ($priority == 2) {
//                    if ($priority == 1) {
//                        if ($planMinConsume->email_consume > $candidatePlan->currentConsume()->email_consume)
//                            $planMinConsume = $candidatePlan->currentConsume();
//                    }
//                } else {
//                    if ($planMinConsume->email_consume > $candidatePlan->currentConsume()->sms_email && $planMinConsume->sms_consume > $candidatePlan->currentConsume()->sms_consume)
//                        $planMinConsume = $candidatePlan->currentConsume();
//                }
//            }
//        }
//        return $planMinConsume;
//
//    }

    /**
     * Verifica se o candidato tem um plano específico
     * @param Plan $plan
     * @return bool
     */
//    public function hasPlan(Plan $plan)
//    {
//        foreach ($this->plans as $planCandidate) {
//            if ($planCandidate->id == $plan->id)
//                return true;
//        }
//        return false;
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listMarketing(){
        return $this->hasMany(ListMarketing::class);
    }
    public function hasListMarketing(ListMarketing $listMarketing){
        $listMarketings = $this->listMarketing()->get();
        return $listMarketings->contains('id',$listMarketing->id);
    }

    /**
     * Retorna todos os senders
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function senders(){
        return $this->hasMany(Sender::class);

    }

    /**
     * @param Sender $sender
     * @return mixed
     */
    public function hasSender(Sender $sender){
        $senders = $this->senders()->get();
        return $senders->contains('id',$sender->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates(){
        return $this->hasMany(TemplateEmailMarketing::class);
    }

    /**
     * @param TemplateEmailMarketing $templateEmailMarketing
     * @return mixed
     */
    public function hasTemplate(TemplateEmailMarketing $templateEmailMarketing){
        $templateEmailMarketings = $this->templates()->get();
        return $templateEmailMarketings->contains('id',$templateEmailMarketing->id);
    }


}
