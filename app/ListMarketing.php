<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListMarketing extends Model
{
    protected $table = 'list_marketing';
    protected $fillable = ['recipient_count','candidate_id','name'];

    public function contacts(){
        return $this->belongsToMany(ContactMarketing::class);
    }
    public function hasContact(ContactMarketing $contactMarketing){
        $contacts = $this->contacts()->get();
        return $contacts->contains('id',$contactMarketing->id);
    }
}
