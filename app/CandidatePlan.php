<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidatePlan extends Model
{
    protected $table = 'candidate_plan';
    protected $fillable = ['plan_id', 'candidate_id', 'automatic_renew', 'hiring_start', 'hiring_end'];

    public function planConsumes(){
        return $this->hasMany(PlanConsume::class);
    }
    public function currentConsume(){
        $planConsumes = $this->hasMany(PlanConsume::class);
        return $planConsumes->orderBy('start_consume', 'desc')->first();
    }

}
