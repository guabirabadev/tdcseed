<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Tweet extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'date_creation',
        'id_tweet',
        'id_str',
        'text',
        'hashtags',
        'symbols',
        'user_mentions',
        'urls',
        'source'
    ];

}
