<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateEmailMarketing extends Model
{
    protected $fillable = ['name','description','content_type','content_value','candidate_id'];
}
