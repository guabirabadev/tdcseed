<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMarketingListMarketing extends Model
{

    protected $fillable = ['list_marketing_id','contact_marketing_id'];

}
