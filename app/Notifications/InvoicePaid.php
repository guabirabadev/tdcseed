<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Facebook\Attachment\Button;
use NotificationChannels\Facebook\FacebookChannel;
use NotificationChannels\Facebook\FacebookMessage;
use NotificationChannels\Facebook\NotificationType;

class InvoicePaid extends Notification
{
    use Queueable;

    public function __construct()
    {
        //
    }


    public function via($notifiable)
    {
        return [FacebookChannel::class];
    }

    public function toFacebook($notifiable)
    {
        $url = url('/invoice/' . $this->invoice->id);

        return FacebookMessage::create()
            ->to($this->user->fb_messenger_user_id) // Optional
            ->text('One of your invoices has been paid!')
            ->notificationType(NotificationType::REGULAR) // Optional
            ->buttons([
                Button::create('Faça nos uma visita', $url)->isTypeWebUrl(),
                Button::create('Ligue para min!', '+1(212)555-2368')->isTypePhoneNumber(),
                Button::create('Iniciar conversa', ['invoice_id' => $this->invoice->id])->isTypePostback() // Custom payload sent back to your server
            ]); // Buttons are optional as well.
    }
}
