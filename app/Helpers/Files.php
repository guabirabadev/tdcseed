<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Config;

class Files
{
    /**
     * Get file object
     * @var object
     */
    public $file;

    /**
     * Get file name
     * @var string
     */
    public $file_name;

    /**
     * Get file name on disk storage
     * @var string
     */
    public $file_disk;

    /**
     * Get file directory
     * @var string
     */
    public $file_dir;

    /**
     * Get file path (disk or hosting)
     * @var string
     */
    public $file_path;

    /**
     * URL to file
     * @var string
     */
    public $file_url;

    /**
     * File extension
     * @var string
     */
    public $file_ext;

    /**
     * File Size
     * @var string
     */
    public $file_size;

    /**
     * File mime type
     * @var string
     */
    public $file_mime;

    /**
     * Files constructor.
     * @param $file
     */
    public function __construct($file )
    {
        $this->file         = $file;
        $this->file_name    = $this->getFileName();
        $this->file_disk    = $this->getFileDisk();
        $this->file_path    = $this->getFilePath();
        $this->file_dir     = $this->getFileDir();
        $this->file_url     = $this->getFileUrl();
        $this->file_ext     = $this->getFileExt();
        $this->file_size    = $this->getFileSize();
        $this->file_mime    = $this->getFileMime();
    }

    /**
     * @return array
     */
    public function getFileData()
    {
        return [
            'file_name' => $this->file_name,
            'file_disk' => $this->file_disk,
            'file_path' => $this->file_path,
            'file_url'  => $this->file_url,
            'file_ext'  => $this->file_ext,
            'file_size' => $this->file_size,
            'file_mime' => $this->file_mime
        ];
    }

    /**
     * @return mixed
     */
    public function token()
    {
        return UUID::generate($this->file_name, 24);
    }

    /**
     * @return mixed
     */
    public function path()
    {
        return Config::get('filesystems.disks.uploads.root');
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->file->getClientOriginalName();
    }

    /**
     * @return string
     */
    public function getFileDisk()
    {
        return $this->token() . '.' . $this->getFileExt();
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        $dateYear = UUID::generate(date('Y'), 5);
        $dateMonth = UUID::generate(date('m'), 5);
        $ds = DIRECTORY_SEPARATOR;

        return $dateYear . $ds . $dateMonth . $ds . $this->file_disk;
    }

    /**
     * @return mixed
     */
    public function getFileDir()
    {
        $path = $this->file_path;
        $fileName = $this->file_disk;

        return $this->path() . str_replace( $fileName, '', $path );
    }

    /**
     * @return string
     */
    public function getFileUrl()
    {
        $path = $this->file_path;
        return str_replace('\\', '/', $path );
    }

    /**
     * @return mixed
     */
    public function getFileExt()
    {
        return $this->file->getClientOriginalExtension();
    }

    /**
     * @return mixed
     */
    public function getFileSize()
    {
        return $this->file->getClientSize();
    }

    /**
     * @return mixed
     */
    public function getFileMime()
    {
        return $this->file->getMimeType();
    }
}