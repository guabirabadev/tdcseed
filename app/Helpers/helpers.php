<?php

if(! function_exists( 'search_child' ) ) {
    /**
     * Search recursive
     * @param $arr
     * @param $id
     * @return bool|mixed
     */
    function search_child( $arr, $id ) {

        if( isset($arr) ) {
            foreach( $arr as $item ) {
                if( $item['id'] == $id ) {
                    $result = true;
                } else {
                    $result = search_child( $item['hierarchy'], $id );
                }
            }
        }

        return (isset($result) ? $result : false);
    }
}

if(! function_exists( 'str_file' ) ) {
    function str_file( $file ) {
        if( isset( $file ) ) {
            $arr = [
                'file_name' => $file->getClientOriginalName(),
                'file_disk' => '',
                'file_path' => '',
                'file_url' => config('app.url') . '/images/',
                'file_ext' => $file->getClientOriginalExtension(),
                'file_size' => $file->getClientSize(),
                'file_mime' => $file->getMimeType()
            ];
        }

        return ( isset( $new ) ? $new : null );
    }
}