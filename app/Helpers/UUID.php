<?php


namespace App\Helpers;

class UUID
{
    /**
     * @param  string $string
     * @param  string $limit
     * @return string
     */
    public static function generate( $string = '', $limit = false, $unique = true)
    {
        $UUID = md5(uniqid($string, $unique));
        if( $limit ) {
            return str_replace('...', '', str_limit( $UUID, $limit ));
        } else {
            return $UUID;
        }
    }

    /**
     * @param string $string
     * @param int $limit
     * @return mixed|string
     */
    static public function unique($string = '', $limit = 32 )
    {
        $UUID = uniqid(md5($string), true);
        if( $limit ) {
            return str_replace('...', '', str_limit( $UUID, $limit ));
        } else {
            return $UUID;
        }
    }

    /**
     * @param  string $string
     * @param  string $limit
     * @return string
     */
    public static function key( $string = '', $limit = false )
    {
        $UUID = md5($string);
        if( $limit ) {
            return str_replace('...', '', str_limit( $UUID, $limit ));
        } else {
            return $UUID;
        }
    }

    /**
     * @param null $limit
     * @return string
     */
    static public function number($limit = null )
    {

        $uuid = sprintf('%05d%05d%05d',
            mt_rand( 0, 99999),
            mt_rand( 0, 99999),
            mt_rand( 0, 99999)
        );


        if( isset($limit) ) {
            $limited = str_limit($uuid, ($limit - 6));
            $return = str_replace('...', '', $limited);
        } else {
            $return = $uuid;
        }

        return $return . date('mY');
    }
}
