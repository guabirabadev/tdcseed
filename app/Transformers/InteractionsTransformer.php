<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Interactions;

/**
 * Class InteractionsTransformer
 * @package namespace App\Transformers;
 */
class InteractionsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Interactions entity
     * @param \Interactions $model
     *
     * @return array
     */
    public function transform(Interactions $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
