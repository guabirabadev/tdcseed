<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consume extends Model
{

    protected $table = "consume";
    protected $fillable = ['description_consume','sms_consume','email_consume','plan_consume_id','created_at'];
}
