<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook_poster' => [
        'app_id'    => getenv('FACEBOOK_APP_ID'),
        'app_secret' => getenv('FACEBOOK_APP_SECRET'),
        'access_token'    => getenv('FACEBOOK_ACCESS_TOKEN'),
    ],
    'facebook' => [
        'page-token' => env('FACEBOOK_PAGE_TOKEN', 'EAAEITGBoQEkBAB8i2gRdkmXcOchSvGaE3Ai6C1ajoPPr1U1ZCTe6ZCZAk2OCGQPGp4rlw7LGDRO1nZAy6chhTeHZBg8OFYu5gkWR6TvYeWohWwOZBB6ZBWAejYJVnEFd5tZChRKOdkMZASZBX4BIt2x7ZCoZBA7K6rkuBVcZCLeZBFfSziZC8Ri9Sjyz53EWFYEylPZAW9IZD
')
    ],

    'botman' => [
//        'hipchat_urls' => [
//            'YOUR-INTEGRATION-URL-1',
//            'YOUR-INTEGRATION-URL-2',
//        ],
        'nexmo_key' => 'YOUR-NEXMO-APP-KEY',
        'nexmo_secret' => 'YOUR-NEXMO-APP-SECRET',
        'microsoft_bot_handle' => 'YOUR-MICROSOFT-BOT-HANDLE',
        'microsoft_app_id' => 'YOUR-MICROSOFT-APP-ID',
        'microsoft_app_key' => 'YOUR-MICROSOFT-APP-KEY',
        'slack_token' => 'YOUR-SLACK-TOKEN-HERE',
        'telegram_token' => 'YOUR-TELEGRAM-TOKEN-HERE',
        'facebook_token' => 'EAAEITGBoQEkBAGuXZAAvo6SAdKnTmcXHvZApMSXZCrH0sEfqvVv66kL6mfLteEj2K3T3tyXsY5cuOdFe5ZCfsI9NhZAl9ead64Ms51to2fRzr4pTTdZBGZC2nC1Yq6DUjoDKxUrbilGW8i9Lfa4aF323kXgQo9Q6VEZD',
        'facebook_app_secret' => '159163329fa49089a75ad2a0d5662ff6', // Optional - this is used to verify incoming API calls,
        'wechat_app_id' => 'YOUR-WECHAT-APP-ID',
        'wechat_app_key' => 'YOUR-WECHAT-APP-KEY',
    ],
];
