<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Intentions of voting
    |--------------------------------------------------------------------------
    */
    'intentions' => [
        'not_questioned' => 'Não questionado',
        'undecided' => 'Indeciso',
        'confirmed' => 'Confirmed',
        'denied' => 'Negado'
    ],

    /*
    |--------------------------------------------------------------------------
    | Education types and descriptions
    |--------------------------------------------------------------------------
    */
    'educations' => [
        'fundamental_complete' => 'Ensino Fundamental Completo',
        'fundamental_incomplete' => 'Ensino Fundamental Incompleto',
        'studying_incomplete' => 'Cursando Ensino Fundamental',
        'full_high_school' => 'Ensino Médio Completo',
        'incomplete_high_school' => 'Ensino Médio Incompleto',
        'studying_high_school' => 'Cursando Ensino Médio',
        'superior_complete' => 'Superior Completo',
        'superior_incomplete' => 'Superior Incompleto',
        'studying_superior' => 'Cursando Superior',
        'illiterate' => 'Analfabeto'
    ]

];
