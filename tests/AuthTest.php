<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        // Testando Insert com a permissão de insert
        echo "\n";
        echo "Testando Insert com a permissão de insert ";
        $User = \App\User::find(1);
        $response = $this->actingAs($User)
            ->get('/insert');
        $response->assertResponseStatus(200)
            ->assertJson(json_encode(['msg'=>'ok']));
        echo ", Resultado OK";

        //Testando Update com a permissão de update
        echo "\n";
        echo "Testando Update com a permissão de update";
        $User = \App\User::find(2);
        $response = $this->actingAs($User)
            ->get('/update');
        $response->assertResponseStatus(200)
            ->assertJson(json_encode(['msg'=>'ok']));
        echo ", Resultado OK";
        echo "\n";


        //Testando Update com a permissão de insert

        echo "Testando Update com a permissão de insert";
        $User = \App\User::find(2);
        $response = $this->actingAs($User)
            ->get('/insert');
        $response->assertResponseStatus(403);
        echo ", Resultado OK";

        // Testando Insert com a permissão de update

        echo "\n";
        echo "Testando Insert com a permissão de update";
        $User = \App\User::find(1);
        $response = $this->actingAs($User)
            ->get('/update');
        $response->assertResponseStatus(403);
        echo ", Resultado OK";
        echo "\n";

        //Testando Admin com a permissão de insert
        echo "Testando Admin com a permissão de insert";
        $User = \App\User::find(4);
        $response = $this->actingAs($User)
            ->get('/insert');
        $response->assertResponseStatus(200)
            ->assertJson(json_encode(['msg'=>'ok']));
        echo ", Resultado OK";
        echo "\n";

        //Testando Admin com a permissão de update
        echo "Testando Admin com a permissão de update";
        $User = \App\User::find(4);
        $response = $this->actingAs($User)
            ->get('/update');
        $response->assertResponseStatus(200)
            ->assertJson(json_encode(['msg'=>'ok']));
        echo ", Resultado OK";
        echo "\n";
    }
}
